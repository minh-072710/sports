<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>

  <link rel="stylesheet" type="text/css" href="/win/css/SpoqaHanSansNeo.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/animate.min.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/sweetalert2.css?v=1" media="all">
  <link rel="stylesheet" type="text/css" href="/win/plugins/swiperjs/css/swiper-core.min.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/aos.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/jquery.modal.min.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/slick.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/plugins/odometer/css/odometer-theme-car.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/hamburger.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/reset.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/icomoon.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/style.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/main.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/layout.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/modal.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/common.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/sports.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/mini.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/mobile.css?v=1">
  <link rel="stylesheet" type="text/css" href="/win/css/daterangepicker.css?v=1">

  <script src="/win/js/jquery-3.6.1.min.js?v=1"></script>
  <script src="/win/js/jquery-ui-1.12.1.js?v=1"></script>
  <script src="/win/js/jquery.ui.touch-punch.js?v=1"></script>
  <script src="/win/js/moment-with-locales.min.js?v=1"></script>
  <script src="/win/js/aos.js?v=1"></script>

  <script type="text/javascript" src="/win/js/jquery.modal.min.js?v=1"></script>
  <script type="text/javascript" src="/win/js/slick.min.js?v=1"></script>
  <script type="text/javascript" src="/win/js/marquee.js?v=1"></script>
  <script type="text/javascript" src="/win/js/jquery.form.min.js?v=1"></script>
  <script type="text/javascript" src="/win/js/daterangepicker.min.js?v=1"></script>
  <script type="text/javascript" src="/win/js/parsley.remote.min.js?v=1"></script>
  <script type="text/javascript" src="/win/js/lazyload.min.js?v=1"></script>
  <script type="text/javascript" src="/win/plugins/tinymce/tinymce.min.js?v=1"></script>
  <script type="text/javascript" src="/win/js/jquery.easing.min.js?v=1"></script>
  <script type="text/javascript" src="/win/js/html2canvas.min.js?v=1"></script>
  <script type="text/javascript" src="/win/js/canvas2image.js?v=1"></script>
  <script type="text/javascript" src="/win/js/sweetalert2.js?v=1"></script>
  <script type="text/javascript" src="/win/js/datepicker.combos.js?v=1"></script>
  <script type="text/javascript" src="/win/js/pagination.js?v=1"></script>
  <script type="text/javascript" src="/win/js/jquery.serialize-object.min.js?v=1"></script>
  <script type="text/javascript" src="/win/js/custom.js?v=1"></script>
  <script type="text/javascript" src="/win/js/ui_script.js?v=1"></script>
  <script type='text/javascript' src="//cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
  <script type="text/javascript" src="/win/js/sports_bet.js?v=1"></script>
</head>

<body id="top_anchor">
  <!-- <div class="wrapper_loading animated hide">
      <div class="loading-box">
        <div class="content">
          <div class="loader-circle"></div>
          <div class="loader-line-mask">
            <div class="loader-line"></div>
          </div>
          <div class="logo animated jump">
            <img src="/win/images/common/favicon.png">
          </div>
          <h1 class="loader_sports">
            <span>L</span>
            <span style="animation-delay: 0.2s">O</span>
            <span style="animation-delay: 0.4s">A</span>
            <span style="animation-delay: 0.6s">D</span>
            <span style="animation-delay: 0.8s">I</span>
            <span style="animation-delay: 1.0s">N</span>
            <span style="animation-delay: 1.2s">G</span>
          </h1>
        </div>
      </div>
    </div> -->
  <div class="page-loader">
    <div class="loader">
      <div class="triple-spinner"></div>
      <div class="spinner"></div>
    </div>
  </div>
  <script type="text/javascript">
    window.addEventListener('load', function() {
      setTimeout(function() {
        var pageLoader = document.querySelector('.page-loader');
        if (pageLoader) {
          pageLoader.style.transition = 'opacity 0.5s';
          pageLoader.style.opacity = 0;

          setTimeout(function() {
            pageLoader.style.display = 'none';
          }, 500);
        }
      }, 500);
    });
  </script>
  <style>
    .page-loader {
      width: 100%;
      height: 100vh;
      position: absolute;
      background: rgba(0, 0, 0, 0.6);
      /* 272727 */
      z-index: 1000;
    }

    .page-loader .loader {
      width: 130px;
      height: 130px;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
    }

    .spinner {
      /* position: relative; */
      /* top: 50%;
        margin: 0 auto; */
      width: 90px;
      height: 90px;
      /* background-image: url('/images/win1/img_loading_1.png'); */
      background-image: url('/images/win1/common/logo.png');
      background-repeat: no-repeat;
      background-size: cover;
      background-position: center center;

      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
    }

    .triple-spinner {
      display: block;
      position: relative;
      width: 125px;
      height: 125px;
      border-radius: 50%;
      border: 4px solid transparent;
      border-top: 4px solid #ff5722;
      animation: spin 2s linear infinite
    }

    .triple-spinner::before,
    .triple-spinner::after {
      content: "";
      position: absolute;
      border-radius: 50%;
      border: 4px solid transparent;
    }

    .triple-spinner::before {
      top: 5px;
      left: 5px;
      right: 5px;
      bottom: 5px;
      border-top-color: #FF9800;
      -webkit-animation: spin 3s linear infinite;
      animation: spin 3.5s linear infinite;
    }

    .triple-spinner::after {
      top: 15px;
      left: 15px;
      right: 15px;
      bottom: 15px;
      border-top-color: #FFC107;
      -webkit-animation: spin 1.5s linear infinite;
      animation: spin 1.75s linear infinite;
    }

    -webkit-@keyframes spin {
      -webkit-from {
        -webkit-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        transform: rotate(0deg);
      }

      -webkit-to {
        -webkit-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    @-webkit-keyframes spin {
      from {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
      }

      to {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    @keyframes spin {
      from {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
      }

      to {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    /* .sports-betting-wrap .sports-container>.main .split .folder-wrap {
        height: calc(100vh - 7px - 7px - 7px);
        padding-bottom: 80px;
      } */
    @media screen and (max-width: 990px) {
      #sports_live .sports-container>.main .split #main-selector {
        padding-bottom: 20px;
      }
    }

    #sports_live .sports-betting-wrap .sports-container>.main .split .folder-wrap .folders:nth-of-type(2),
    #sports_live .sports-betting-wrap .sports-container>.main .split .folder-wrap .folders:nth-of-type(3) {
      padding-bottom: 100px;
    }
  </style>
  <div id="betLoading" style="display:none;">
    <img src="/win/images/common/Spinner.svg">
    <div class="betting-span"><span>배팅중... <span id="bettingSecond">0</span>초</span></div>
  </div>
  <script type="text/javascript">
    var g_autoAccessCode = "";

    var myCash = 0;
    var bet_delay = 0;
    var game_type = '2';
    var serverTimeStamp = 0;
    var sportsWebsocket;
    var virtualWebSocket;
    var sportsAjaxInterval;
    var sportsRequestPacket = {
      "sports_code": "",
      "league_code": "",
      "game_type": game_type,
      "family": 0,
      "page_start": 0,
      "page_length": 15,
      "search": ""
    };
    var virtualGameRequestPacket = {
      "game_type": "",
    }
    var m_sportsBetList = new BetList();
    var $sportsQnaTable = '';
    var tmpShowSportsName = '';
    var familyMap = new Map();

    $(document).on('click', '[data-role="accordion:switcher"]', function() {
      $(this).toggleClass("accordion-opened");
      return false;
    })

    var virtualWebSocket;

    $(function() {


      openSportsWebSocket();

      setInterval(
        function() {
          /* if (serverTimeStamp > 0) {
            let serverTimeDate = new Date(serverTimeStamp * 1000);
            let year = serverTimeDate.getFullYear();
            let month = fillZero(2, parseInt(serverTimeDate.getMonth() + 1));
            let day = fillZero(2, serverTimeDate.getDate());
            let hour = fillZero(2, parseInt(serverTimeDate.getHours())); 
            let min = fillZero(2, parseInt(serverTimeDate.getMinutes()));
            let sec = fillZero(2, serverTimeDate.getSeconds());
            let time = year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
            serverTimeStamp ++;
          } */

          const date = new Date();
          let year = date.getFullYear();
          let month = fillZero(2, parseInt(date.getMonth() + 1));
          let day = fillZero(2, date.getDate());
          let hour = fillZero(2, parseInt(date.getHours()));
          let min = fillZero(2, parseInt(date.getMinutes()));
          let sec = fillZero(2, date.getSeconds());
          let time = year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
          $(".game_box #time-limit span").html(time);
          $(".sports-category .sports-time span").html(time);
        }, 1000);
    });

    function fillZero(width, padString) {
      padString = String(typeof padString !== 'undefined' ? padString : ' ');
      return padString.padStart(width, '0');
    }

    function openSportsWebSocket() {
      if (sportsWebsocket !== undefined && sportsWebsocket.readyState !== WebSocket.CLOSED && sportsWebsocket.readyState !== WebSocket.CLOSING) {
        return;
      }
      sportsWebsocket = new WebSocket("wss://ws.totos999.com");

      sportsWebsocket.onopen = function(event) {
        if (event.data === undefined) {
          return;
        }
      };

      sportsWebsocket.onmessage = function(event) {
        if (event.data == 'welcome!!!') {
          // sendMsgSportsData();
        } else {
          const jData = JSON.parse(event.data);
          showSportsCntInfo(jData);

          if (typeof rebuildSportsGameInfo == "function") {
            rebuildSportsGameInfo(jData.data);
          }
        }
      };

      sportsWebsocket.onclose = function(event) {

      }
    }

    function closeSportsWebSocket() {
      if (sportsWebsocket) {
        sportsWebsocket.close();
      }
    }

    /**
     * 스포츠경기 자료 요청
     */
    function sendMsgSportsData() {
      clearInterval(sportsAjaxInterval);
      if (sportsWebsocket.readyState === WebSocket.OPEN) {
        sportsRequestPacket.user_id = '';
        sportsWebsocket.send(JSON.stringify(sportsRequestPacket));
        if (sportsRequestPacket.game_type != 3) { // 실시간
          sportsAjaxInterval = setInterval(function() {
            sportsWebsocket.send(JSON.stringify(sportsRequestPacket));
          }, 30000);
        } else {
          sportsAjaxInterval = setInterval(function() {
            sportsWebsocket.send(JSON.stringify(sportsRequestPacket));
          }, 5000);
        }
      } else {
        setTimeout(() => {
          sportsWebsocket.send(JSON.stringify(sportsRequestPacket));
          if (sportsRequestPacket.game_type != 3) { //실시간
            sportsAjaxInterval = setInterval(function() {
              sportsWebsocket.send(JSON.stringify(sportsRequestPacket));
            }, 30000);
          } else {
            sportsAjaxInterval = setInterval(function() {
              sportsWebsocket.send(JSON.stringify(sportsRequestPacket));
            }, 5000);
          }
        }, 30000);
      }
    }

    //스포츠 국가, 리그 목록 추가
    function showSportsCntInfo(jData) {
      familyMap.clear();
      if (document.getElementById("left-selector") != undefined) {
        if (jData.data && jData.data.cntInfo) {
          serverTimeStamp = jData.data.curTime;
          data = jData.data.cntInfo;
          let totalSportsCnt = 0;
          for (let i = 0; i < data.length; i++) {
            let sportsData = data[i];
            let sportsNameEn = sportsData.sportsNameEn;
            let prevTotalCnt = $(".total-count-" + sportsNameEn).text();

            if (prevTotalCnt != sportsData.count) {
              $(".total-count-" + sportsNameEn).addClass("shine-number");
            } else {
              $(".total-count-" + sportsNameEn).removeClass("shine-number");
            }
            $(".total-count-" + sportsNameEn).text(sportsData.count);
            let prevSportsCnt = $("#main-selector .selector .category-count-" + sportsNameEn).text();
            if (prevSportsCnt != sportsData.count) {
              $("#main-selector .selector .category-count-" + sportsNameEn).addClass("shine-number");
            } else {
              $("#main-selector .selector .category-count-" + sportsNameEn).removeClass("shine-number");
            }
            $("#main-selector .selector .category-count-" + sportsNameEn).text(sportsData.count);
            totalSportsCnt += parseInt(sportsData.count);
            if (sportsData.nationCntList) {
              appendSportsCntInfo(sportsNameEn, sportsData.nationCntList);
            }
          }
          let prevTotalSportsCnt = $("#main-selector .selector .category-count-all").text();
          if (prevTotalSportsCnt != totalSportsCnt) {
            $("#main-selector .selector .category-count-all").addClass("shine-number");
          } else {
            $("#main-selector .selector .category-count-all").removeClass("shine-number");
          }
          $("#main-selector .selector .category-count-all").text(totalSportsCnt);
        }
      }

      $(".family-count").html("(0)");

      for (let [key, value] of familyMap) {
        $('[data-family="' + key + '"]').html("(" + value + ")");
      }
    }

    function appendSportsCntInfo(sportsName, nationCntList) {
      // 삭제
      let $sel = '';
      let arrDelNationEle = [];
      let nationMenuClassName = "nation-menu-" + sportsName;

      $("#left-selector ." + nationMenuClassName).children().each(function() {
        let isExist = false;
        let dataEleClassName = '';
        let firstChildClassName = $(this).attr('class');
        for (let i = 0; i < nationCntList.length; i++) {
          dataEleClassName = "sports-" + sportsName + "-" + nationCntList[i].nationCode;
          if (dataEleClassName == firstChildClassName) {
            isExist = true;
            break;
          }
        }
        if (isExist == false) {
          firstChildClassName = firstChildClassName.replaceAll(' ', '.');
          arrDelNationEle.push(firstChildClassName);
        }
      });

      if (arrDelNationEle.length > 0) {
        for (let i = 0; i < arrDelNationEle.length; i++) {
          $("#left-selector ." + arrDelNationEle[i]).remove();
        }
      }

      // nation 리스트 추가
      for (let i = 0; i < nationCntList.length; i++) {
        let nationInfo = nationCntList[i];
        let nationClassName = "sports-" + sportsName + "-" + nationInfo.nationCode;

        let nationObjs = document.getElementsByClassName(nationClassName);
        if (nationObjs != undefined && nationObjs.length > 0) {
          let leagueCntQueryPath = "." + nationClassName + " .nation-cnt";
          let prevCnt = parseInt($(leagueCntQueryPath).html());

          if (prevCnt != nationInfo.count) {
            $(leagueCntQueryPath).addClass("shine-number");
          } else {
            $(leagueCntQueryPath).removeClass("shine-number");
          }
          $(leagueCntQueryPath).html(nationInfo.count);
          if (nationInfo.leagueCntList) {
            let arrDelLeagueEle = [];
            let leagueMenuQueryPath = "#left-selector ." + nationMenuClassName + " .sports-" + sportsName + "-" + nationInfo.nationCode + " .league-menu-" + sportsName;
            $(leagueMenuQueryPath).children().each(function() {
              let isExist = false;
              let dataEleClassName = '';
              let firstChildClassName = $(this).attr('class');
              for (let k = 0; k < nationInfo.leagueCntList.length; k++) {
                let leagueInfo = nationInfo.leagueCntList[k];
                dataEleClassName = "sports-" + sportsName + "-" + nationInfo.nationCode + "-" + leagueInfo.leagueCode;
                if (firstChildClassName.includes(dataEleClassName)) {
                  isExist = true;
                  break;
                }
              }
              if (isExist == false) {
                firstChildClassName = firstChildClassName.replaceAll(' ', '.');
                arrDelLeagueEle.push(firstChildClassName);
              }
            });
            if (arrDelLeagueEle.length > 0) {
              for (let k = 0; k < arrDelLeagueEle.length; k++) {
                $("#left-selector ." + arrDelLeagueEle[k]).remove();
              }
            }

            for (let k = 0; k < nationInfo.leagueCntList.length; k++) {
              let leagueInfo = nationInfo.leagueCntList[k];

              let tmpLeagueCnt = familyMap.get(leagueInfo.family);
              if (tmpLeagueCnt == undefined) {
                tmpLeagueCnt = leagueInfo.count;
              } else {
                tmpLeagueCnt += leagueInfo.count;
              }
              familyMap.set(leagueInfo.family, tmpLeagueCnt);

              let leagueDivClass = "sports-" + sportsName + "-" + nationInfo.nationCode + "-" + leagueInfo.leagueCode;
              let leagueDivObjs = document.getElementsByClassName(leagueDivClass);
              if (leagueDivObjs != undefined && leagueDivObjs.length > 0) {
                leagueDivClass = "." + leagueDivClass + " .league-cnt";
                let prevLeagueCnt = parseInt($(leagueDivClass).html());

                if (prevLeagueCnt != leagueInfo.count) {
                  $(leagueDivClass).addClass("shine-number");
                } else {
                  $(leagueDivClass).removeClass("shine-number");
                }

                $(leagueDivClass).html(leagueInfo.count);
              } else {
                $sel = `
                    <div data-level="2" data-role="query" data-league-id="${leagueInfo.leagueCode}" class="sports-${sportsName}-${nationInfo.nationCode}-${leagueInfo.leagueCode} league-left-menu" onclick="onClickLeague('${sportsName}', '${nationInfo.nationCode}', '${leagueInfo.leagueCode}')"> ${leagueInfo.leagueName} &nbsp; (<span class="league-cnt">${leagueInfo.count}</span>)</div>
                  `;
                if (k == 0) {
                  $(leagueMenuQueryPath).prepend($sel);
                } else {
                  let prevLeagueDivClass = ".sports-" + sportsName + "-" + nationInfo.nationCode + "-" + nationInfo.leagueCntList[k - 1].leagueCode;
                  $($sel).insertAfter($(prevLeagueDivClass));
                }
              }
            }
          }
        } else {
          $sel = `
              <div data-level="1" class="sports-${sportsName}-${nationInfo.nationCode}">
                <div data-role="accordion:switcher">
                  <div class="left nation_left_menu">
                    <div class="flag-icon">
                      <img src="${nationInfo.imgPath}" alt="">
                    </div>
                    <div class="name"> ${nationInfo.nationName} &nbsp; (<span class="number nation-cnt">${nationInfo.count}</span>) </div>
                  </div>
                  <div class="right"> <i class="icon-iconARupA"></i> </div>
                </div>
                <div data-role="accordion:switchee" class="league-menu-${sportsName}">`;
          if (nationInfo.leagueCntList) {
            for (let j = 0; j < nationInfo.leagueCntList.length; j++) {
              leagueInfo = nationInfo.leagueCntList[j];
              let tmpLeagueCnt = familyMap.get(leagueInfo.family);
              if (tmpLeagueCnt == undefined) {
                tmpLeagueCnt = leagueInfo.count;
              } else {
                tmpLeagueCnt += leagueInfo.count;
              }
              familyMap.set(leagueInfo.family, tmpLeagueCnt);
              $sel += `
                <div data-level="2" data-role="query" data-league-id="${leagueInfo.leagueCode}" class="sports-${sportsName}-${nationInfo.nationCode}-${leagueInfo.leagueCode} league-left-menu" onclick="onClickLeague('${sportsName}', '${nationInfo.nationCode}', '${leagueInfo.leagueCode}')"> ${leagueInfo.leagueName} &nbsp; (<span class="league-cnt">${leagueInfo.count}</span>)</div>
                `;
            }
          }
          $sel += `</div>
              </div>
            `;
          if (i == 0) {
            $("#left-selector ." + nationMenuClassName).prepend($sel);
          } else {
            let prevDivClass = ".sports-" + sportsName + "-" + nationCntList[i - 1].nationCode;
            $($sel).insertAfter($(prevDivClass));
          }
        }
      }
    }

    function appendPickToBetCart(pickInfo) {
      let bonusClassName = '';
      if (pickInfo.fixture_id < 100) {
        bonusClassName = "bonus";
      }
      let $sel = '';
      let statusAttr = 'hidden';
      if (pickInfo.status > 0) {
        statusAttr = '';
      }

      let oldRate = '';
      let newRate = '';

      if (pickInfo.old_rate == '') {
        oldRate = parseFloat(pickInfo.select_rate).toFixed(2);
      } else {
        oldRate = parseFloat(pickInfo.old_rate).toFixed(2);
        newRate = parseFloat(pickInfo.select_rate).toFixed(2);
      }

      $sel = `
        <div class="folder sel-pick-${pickInfo.bet_code}">
          <div class="title ${bonusClassName}">
          <div class="left">
            ${pickInfo.pick_title}
          </div>
          <div class="right">
            <div class="delete" onclick="deletePickInfo('${pickInfo.bet_code}')">
            <span class="icon-iconCross"></span>
            </div>
          </div>
          </div>
          <div class="content">
            <div class="row">
              <div class="option"> ${pickInfo.market_name} </div>
              <div class="new-odds">${newRate}</div>
            </div>
            <div class="row">
              <div class="pick">${pickInfo.select_pick_desc}</div>
              <div class="odds" id="cart_${pickInfo.bet_code}">${oldRate}</div>
            </div>
            </div>
          <div class="icon-iconLock" ${statusAttr}></div>
        </div>`;

      $("#betting_cart .betting-pick-option .folder-wrap").append($sel);
    }

    document.documentElement.style.setProperty('--ver', 'v=74468');

    window.onresize = function() {
      updatePopupDimension();
    }

    function updatePopupDimension() {
      let maxHH = $(document).height() / 2;
      $('.notice_popup_area .popup_wrapper .notice_popup').each(function() {
        let hh = $(this).outerHeight();
        if (maxHH < hh) {
          maxHH = hh;
        }
      });
      $('.notice_popup_area .popup_wrapper').attr('style', "--popup-area-height: " + maxHH + "px");
    }

    onHashChange();
    window.addEventListener('popstate', function() {
      onHashChange();
    });

    function onHashChange() {
      if ($('body').hasClass('hamburger-active')) {
        $('body').removeClass('hamburger-active');
      }

      let hash = location.hash || "/main";
      if (hash.startsWith('#')) {
        hash = hash.substring(1);
      }

      refreshMenuItems($('.slide-content-left ul>li>a'), hash);
      refreshMenuItems($('header .center a.item'), hash);
      refreshMenuItems($('footer .footer-list li>a'), hash);

      $('.slide-content-left').removeClass('active');
      $('#app, header').removeClass('slide-left');

      if (!hash.includes("/main") &&
        !hash.includes("/game/sports") &&
        !hash.includes("/header/bulletin")
      ) {
        if (!chkSignedIn()) hash = "/main";
      }

      let removeAnimSlide = false;
      let oldHash = $('#app').data('hash');
      if (oldHash) {
        let baseUrls = this.baseUrls || [
          '/header/notice',
          '/header/bulletin',
          '/header/event',
          '/mypage/children',
          '/mypage/qna',
          '/mypage/letter',
          '/game/betlog',
        ];
        for (const baseUrl of baseUrls) {
          if (oldHash.startsWith(baseUrl) && hash.startsWith(baseUrl)) {
            removeAnimSlide = true;
            break;
          }
        }
      }

      if (!hash.includes("/game/sports/")) {
        clearInterval(sportsAjaxInterval);
      }

      if (!hash.includes("game/virtual")) {
        closeVirtualWebSocket();
      }

      return;
      loading.open();

      ajaxGetSend('/front' + hash, {}, function(data) {

        $('#app').html(data)
          .data('hash', hash);

        if (removeAnimSlide) {
          $('#app').find('.slideInRight, .slideInLeft')
            .removeClass("slideInRight")
            .removeClass("slideInLeft")
        }

        let parsley = $('#app [data-parsley-validate]').parsley();
        if (parsley) parsley.destroy();
        $('#app [data-parsley-validate]').parsley();

        refreshLazyLoad();
      }, undefined, function() {
        loading.close();
      });
    }

    function refreshMenuItems($elems, hash) {
      $elems.removeClass('active on');
      $elems.each(function() {
        let href = $(this).attr('href') || '';
        if (href.startsWith('#')) {
          href = href.substring(1);
        }
        if (href == '/') href = '';
        if (hash == '/') hash = '';

        if (!href && hash == href) { // home
          // console.log('+ hash: ', hash, 'href: ', href, $(this));
          $(this).addClass('active on');
          return false;
        }
        if (href && hash.startsWith(href)) {
          // console.log('- hash: ', hash, 'href: ', href, $(this));
          $(this).addClass('active on');
          return false;
        }
      })
    }

    function refreshLazyLoad(sel) {
      sel = sel || 'img.lazyload';
      $(sel).lazyload();
    }

    $(function() {
      updatePopupDimension();
      updateCartHeight();

      $('#footer-btn a.menu').click(function(e) {
        $('body').toggleClass('hamburger-active');
      });


      $('#app').on('click', '.tab-btn', function() {
        let wrapper = $(this).parent();
        if (wrapper.hasClass('tabs')) { // .tabs>.tab-btn인 경우에만 active토글을 한다. 기타경우는 active토글이 필요없는 경우
          $(this).siblings().removeClass('active');
          $(this).addClass('active');
        }
        let selLink = $(this).data('link');
        $(selLink).removeClass('hide')
          .siblings('.tab-pane').addClass('hide');
      })


    })

    // s: virtual game

    function openVirtualWebSocket() {
      closeVirtualWebSocket();

      if (virtualWebSocket !== undefined && virtualWebSocket.readyState !== WebSocket.CLOSED && virtualWebSocket.readyState !== WebSocket.CLOSING) {
        return;
      }

      virtualWebSocket = new WebSocket("ws://" + location.host + "/wsvirtual");

      virtualWebSocket.onopen = function(event) {
        virtualWebSocket.send(JSON.stringify(virtualGameRequestPacket));

        if (event.data === undefined) {
          return;
        }
      };

      virtualWebSocket.onmessage = function(event) {
        if (event.data == 'Hello') {} else {
          const jData = JSON.parse(event.data);
          if (typeof rebuildVirtualGameInfo == "function") {
            rebuildVirtualGameInfo(jData.data);
          }
        }
      };

      virtualWebSocket.onclose = function(event) {

      }
    }

    function closeVirtualWebSocket() {
      if (virtualWebSocket) {
        virtualWebSocket.close();
      }
    }
    // -- e: virtual
  </script>
  <script type="text/javascript">
    // Function to update cart height and overflow
    function updateCartHeight() {
      var cart = document.querySelector('#betting_cart');
      var cartContentHeight = cart.scrollHeight; // Total height of content inside the cart
      var maxHeight = parseInt(window.getComputedStyle(cart).maxHeight); // Max height set in CSS

      if (cartContentHeight > maxHeight) {
        cart.style.height = maxHeight + 'px'; // Set height to max height
      } else {
        cart.style.height = 'auto'; // Allow cart to expand if content is smaller than max height
      }
    }

    // Call the function initially and whenever the window is resized
    window.addEventListener('resize', updateCartHeight);
  </script>
  <div id="app">
    <link rel="stylesheet" type="text/css" href="/win/css/inplay.css?v=1">
    <div class="container sports">
      <div class="sports-betting-wrap" id="sports_live">
        <div class="sports-container">
          <div id="left-selector" class="left pc">
            <div class="home-header mob">
              <div class="header-content">
                <div class="left">
                  <a class="left-button" rel="modal:close" onclick="$('#left-selector').addClass('pc')"><i class="icon_left"></i></a>
                </div>
                <div class="conter title">리그 정보</div>
                <div class="right">
                  <span class="icon-iconCross" onclick="$('#left-selector').addClass('pc')"></span>
                </div>
              </div>
            </div>
            <div class="list">
              <div class="search-box clearable">
                <input type="text" name="search" placeholder="검색할 게임을 입력해주세요" onkeyup="if(window.event.keyCode==13){onClickSearch()}">
                <button onclick="clearSearchString()" type="reset">×</button>
              </div>
              <div class="sports-category">
                <div class="sports-time">
                  <span></span>
                </div>
                <div data-level="0">
                  <div data-role="accordion:switcher">
                    <div class="left sport-icon" data-sport="축구">
                      <div class="sports-logo">
                        <img src="/win/images/sports/football.png" alt="">
                      </div>
                      축구
                    </div>
                    <div class="right">
                      <div class="number">
                        (<span class="total-count-football">0</span>)
                      </div>
                      <i class="icon-iconARupA" hover=""></i>
                    </div>
                  </div>
                  <div data-role="accordion:switchee" class="nation-menu-football"></div>
                </div>
                <div data-level="0">
                  <div data-role="accordion:switcher" class="">
                    <div class="left sport-icon" data-sport="야구">
                      <div class="sports-logo">
                        <img src="/win/images/sports/baseball.png" alt="">
                      </div>
                      야구
                    </div>
                    <div class="right">
                      <div class="number">&nbsp; (<span class="total-count-baseball">0</span>)</div>
                      <i class="icon-iconARupA" hover=""></i>
                    </div>
                  </div>
                  <div data-role="accordion:switchee" class="nation-menu-baseball"></div>
                </div>
                <div data-level="0">
                  <div data-role="accordion:switcher">
                    <div class="left sport-icon" data-sport="농구">
                      <div class="sports-logo">
                        <img src="/win/images/sports/basket.png" alt="">
                      </div>
                      농구
                    </div>
                    <div class="right">
                      <div class="number">&nbsp; (<span class="total-count-basketball">0</span>)</div>
                      <i class="icon-iconARupA" hover=""></i>
                    </div>
                  </div>
                  <div data-role="accordion:switchee" class="nation-menu-basketball"></div>
                </div>
                <div data-level="0">
                  <div data-role="accordion:switcher">
                    <div class="left sport-icon" data-sport="배구">
                      <div class="sports-logo">
                        <img src="/win/images/sports/valleyball.png" alt="">
                      </div>
                      배구
                    </div>
                    <div class="right">
                      <div class="number">&nbsp; (<span class="total-count-volleyball">0</span>)</div>
                      <i class="icon-iconARupA" hover=""></i>
                    </div>
                  </div>
                  <div data-role="accordion:switchee" class="nation-menu-volleyball"></div>
                </div>
                <div data-level="0">
                  <div data-role="accordion:switcher">
                    <div class="left sport-icon" data-sport="아이스 하키">
                      <div class="sports-logo">
                        <img src="/win/images/sports/hockey.png" alt="">
                      </div>
                      아이스 하키
                    </div>
                    <div class="right">
                      <div class="number">&nbsp; (<span class="total-count-ice_hockey">0</span>)</div>
                      <i class="icon-iconARupA" hover=""></i>
                    </div>
                  </div>
                  <div data-role="accordion:switchee" class="nation-menu-ice_hockey"></div>
                </div>
                <div data-level="0">
                  <div data-role="accordion:switcher">
                    <div class="left sport-icon" data-sport="e스포츠">
                      <div class="sports-logo">
                        <img src="/win/images/sports/gamepad.png" alt="">
                      </div>
                      e스포츠
                    </div>
                    <div class="right">
                      <div class="number">&nbsp; (<span class="total-count-e_sports">0</span>)</div>
                      <i class="icon-iconARupA" hover=""></i>
                    </div>
                  </div>
                  <div data-role="accordion:switchee" class="nation-menu-e_sports"></div>
                </div>
                <div data-level="0">
                  <div data-role="accordion:switcher">
                    <div class="left sport-icon" data-sport="미식축구">
                      <div class="sports-logo">
                        <img src="/win/images/sports/usa_football.png" alt="">
                      </div>
                      미식축구
                    </div>
                    <div class="right">
                      <div class="number">&nbsp; (<span class="total-count-american_football">0</span>)</div>
                      <i class="icon-iconARupA" hover=""></i>
                    </div>
                  </div>
                  <div data-role="accordion:switchee" class="nation-menu-american_football"></div>
                </div>
              </div>

              <div class="topleague" id="topleague" data-level="0"></div>
            </div>
          </div>
          <div class="main">
            <div class="split">
              <div id="main-selector">
                <div class="mob-main-header mob">
                  <div class="quickbetting">
                    <div class="top"> 퀵 베팅
                      <div class="info icon-iconExclamation"> </div>
                      <div class="money" data-show-target="money">
                        <input type="text" name="money" data-type="comma" value="0" readonly="" data-raw-value="0">
                        <button class="edit-button">편집</button>
                        <button class="select-button hide">선택</button>
                      </div>
                      <div class="setIcon">
                        <span class="icon-icconSET" hidden=""></span>
                      </div>
                    </div>
                    <div class="bottom">
                      <div class="quick-bet-switch" data-role="accordion:switcher">
                        <div class="switch" data-show="money" onclick="switchMobQuickBetStatus()"></div>
                      </div>
                      <div data-role="accordion:switchee">
                        <div class="icon-icconWARNING"> 금액 수정후 베팅해주세요(단폴만 가능) </div>
                      </div>
                      <div></div>
                      <div></div>
                    </div>
                    <div class="league-menu-wrap mob" onclick="$('#left-selector').removeClass('pc')"> 리그 &nbsp;&nbsp;
                      <div class="league-menu">
                        <img src="/win/images/common/lg_menu.png">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="selector">
                  <div class="sports-title pc">
                    <span>스포츠(라이브)</span>
                  </div>
                  <div class="item category-all active" data-query="sport" data-sport="" onclick="showSportsGameByCode('')"></div>
                  <div class="item category-6046" data-query="sport" data-sport="축구" onclick="showSportsGameByCode('6046')">
                    <div class="sports-logo">
                      <img src="/win/images/sports/football.png" alt="축구">
                    </div>
                  </div>
                  <div class="item category-154914" data-query="sport" data-sport="야구" onclick="showSportsGameByCode('154914')">
                    <div class="sports-logo">
                      <img src="/win/images/sports/baseball.png" alt="야구">
                    </div>
                  </div>
                  <div class="item category-48242" data-query="sport" data-sport="농구" onclick="showSportsGameByCode('48242')">
                    <div class="sports-logo">
                      <img src="/win/images/sports/basket.png" alt="농구">
                    </div>
                  </div>
                  <div class="item category-154830" data-query="sport" data-sport="배구" onclick="showSportsGameByCode('154830')">
                    <div class="sports-logo">
                      <img src="/win/images/sports/valleyball.png" alt="배구">
                    </div>
                  </div>
                  <div class="item category-35232" data-query="sport" data-sport="아이스 하키" onclick="showSportsGameByCode('35232')">
                    <div class="sports-logo">
                      <img src="/win/images/sports/hockey.png" alt="아이스 하키">
                    </div>
                  </div>
                  <div class="item category-687890" data-query="sport" data-sport="e스포츠" onclick="showSportsGameByCode('687890')">
                    <div class="sports-logo">
                      <img src="/win/images/sports/gamepad.png" alt="e스포츠">
                    </div>
                  </div>
                  <div class="item category-131506" data-query="sport" data-sport="미식축구" onclick="showSportsGameByCode('131506')">
                    <div class="sports-logo">
                      <img src="/win/images/sports/usa_football.png" alt="미식축구">
                    </div>
                  </div>
                </div>
              </div>
              <div class="folder-wrap" id="odds-folder-wrap">
                <div id="fixture-list">
                  <div class="folders" id="game-folders">
                    <div id="top-hidden" style="height: 0px"></div>
                  </div>
                </div>
                <div class="paging_box"></div>
              </div>
              <div id="market-list" class="pc"></div>
            </div>
          </div>
          <div id="right-container" class="right">
            <div id="betting_cart" class="ui-draggable ui-draggable-handle hidden">
              <div class="home-header mob">
                <div class="header-content">
                  <div class="left">
                    <a class="left-button" rel="modal:close" onclick="$('#betting_cart').addClass('hidden')"><i class="icon_left"></i></a>
                  </div>
                  <div class="center title">
                    베팅 카트
                  </div>
                  <div class="right">
                    <span class="icon-iconCross" onclick="$('#betting_cart').addClass('hidden')"></span>
                  </div>
                </div>
              </div>
              <div class="quickbetting pc">
                <div class="top"> 퀵 베팅 <div class="info icon-iconExclamation"> </div>
                  <div class="money" data-show-target="money">
                    <input type="text" name="money" data-type="comma" value="0" readonly="" data-raw-value="0">
                    <button class="edit-button">편집</button>
                    <button class="select-button hide">선택</button>
                  </div>
                  <div class="setIcon">
                    <span class="icon-icconSET" hidden=""></span>
                  </div>
                </div>
                <div class="bottom">
                  <div class="quick-bet-switch" data-role="accordion:switcher">
                    <div class="switch" data-show="money" onclick="switchPCQuickBetStatus()"></div>
                  </div>
                  <div data-role="accordion:switchee">
                    <div class="icon-icconWARNING"> 금액 수정후 베팅해주세요(단폴만 가능) </div>
                  </div>
                  <div></div>
                  <div></div>
                </div>
              </div>
              <div class="selector" selected="">
                <div class="item selected" data-rel="true">
                  베팅카트 <span class="number bet-cart-cnt"></span>
                </div>
                <div class="item" data-rel="false">
                  베팅 기록 <span class="number"></span>
                </div>
              </div>
              <div class="options">
                <div class="option betting-pick-option">
                  <div class="cart">
                    <div class="delete-all"> 모두 삭제
                      <span class="icon-iconCross" onclick="clearBetCart();"></span>
                    </div>
                    <div class="scroll">
                      <div id="betting-cart">
                        <div class="folder-wrap">
                        </div>
                      </div>
                      <div class="monitor">
                        <div class="selected">
                          <div class="left">선택폴더</div>
                          <div class="right"> <span class="number bet-cart-cnt">0</span>폴더 </div>
                        </div>
                        <div class="state-wrap">
                          <div class="state bet-odds">
                            <div class="key">배당률 합계</div>
                            <div class="value">0.00</div>
                          </div>
                          <div class="state bet-money">
                            <div class="key">당첨 예상금액</div>
                            <div class="value"> <span class="cash-number">0</span>&nbsp;원</div>
                          </div>
                          <div class="state">
                            <div class="key">베팅 금액</div> <input type="text" name="input_bet_cash" data-type="comma" class="value" placeholder="금액을 입력해주세요." data-raw-value="0">
                          </div>
                        </div>
                        <div class="executer-wrap">
                          <div class="executer" data-type="false" onclick="bettingMoneyPlus(5000)"> + 5천 </div>
                          <div class="executer" data-type="false" onclick="bettingMoneyPlus(10000)"> + 1만 </div>
                          <div class="executer" data-type="false" onclick="bettingMoneyPlus(50000)"> + 5만 </div>
                          <div class="executer" data-type="false" onclick="bettingMoneyPlus(100000)"> + 10만 </div>
                          <div class="executer" data-type="false" onclick="bettingMoneyPlus(500000)"> + 50만 </div>
                          <!-- <div class="executer" data-type="true" onclick="clearMoney()"> RESET </div> -->
                        </div>
                        <div class="executer-wrap func">
                          <div class="executer" data-type="true" onclick="clearMoney()"> RESET </div>
                        </div>
                        <div class="executer-wrap">
                          <div class="executer" data-type="false" onclick="bettingAllIn()"> 최대 베팅 </div>
                          <div class="executer" data-type="false" onclick="checkBetCartRate()"> 배당 갱신 </div>
                          <!-- <div class="executer" data-type="true" onclick="sendBetCart()"> 베팅하기 </div> -->
                        </div>
                        <div class="executer-wrap func">
                          <div class="executer" data-type="true" onclick="sendBetCart()"> 베팅하기 </div>
                        </div>
                      </div>
                      <div class="restriction-wrap">
                        <div class="restriction">
                          <div class="key">보유금액</div>
                          <div class="value"> <span class="number userCashAmount">0</span> &nbsp;원</div>
                        </div>
                        <div class="restriction">
                          <div class="key">보유포인트</div>
                          <div class="value"> <span class="number userCashPoint">0</span> &nbsp;P</div>
                        </div>
                      </div>
                      <div class="restriction-wrap">
                        <div class="restriction">
                          <div class="key">단폴 최소 베팅금액</div>
                          <div class="value single-min-cash-bet"> <span class="number">0</span> &nbsp;원</div>
                        </div>
                        <div class="restriction">
                          <div class="key">단폴 최대 베팅금액</div>
                          <div class="value single-max-cash-bet"> <span class="number">0</span> &nbsp;원 </div>
                        </div>
                        <div class="restriction">
                          <div class="key">단폴 최대 당첨금액</div>
                          <div class="value single-max-cash-win"> <span class="number">0</span> &nbsp;원 </div>
                        </div>
                        <div class="restriction">
                          <div class="key">다폴 최소 베팅금액</div>
                          <div class="value multi-min-cash-bet"> <span class="number">0</span> &nbsp;원</div>
                        </div>
                        <div class="restriction">
                          <div class="key">다폴 최대 베팅금액</div>
                          <div class="value multi-max-cash-bet"> <span class="number">0</span> &nbsp;원 </div>
                        </div>
                        <div class="restriction">
                          <div class="key">다폴 최대 당첨금액</div>
                          <div class="value multi-max-cash-win"> <span class="number">0</span> &nbsp;원 </div>
                        </div>
                      </div>
                      <div class="close-cart">
                        <span class="desc"> 베팅을 그만두시겠습니까? </span>
                        <a class="close-button" rel="modal:close">취소</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="option betting-list-option">
                  <div class="history">
                    <div class="scroll">
                      <div class="folder-wrap"></div>
                      <div class="more" onclick="getBettingList()">
                        <span>더보기</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="betting_cart_opener" betting_cart="" class="ui-draggable ui-draggable-handle">
              <b>0</b>
            </div>
          </div>
        </div>
        <div id="btn_top" class="hide"></div>
      </div>
    </div>
    <script type="text/javascript">
      var mainMarketMap = new Map([

        ['6046', '1'],

        ['48242', '226'],

        ['154914', '226'],

        ['154830', '52'],

        ['35232', '1'],

        ['131506', '226'],

        ['687890', '52'],

      ]);
      var oldDetailInfoList = [];
      var showCnt = 0;
      var pg_start = 0;
      var minBetCash = 0;

      var singleMaxBetCash = parseFloat('0' || 0);
      var singleMaxWinCash = parseFloat('0' || 0);
      var multiMaxBetCash = parseFloat('0' || 0);
      var multiMaxWinCash = parseFloat('0' || 0);

      var showFixtureId = -1;
      var tmpShowGameCnt = 0;

      $(document).on('wheel', '#main-selector', function(e) {
        $(this).children('.selector').scrollLeft(e.originalEvent.deltaY * 10);
      })

      $(function() {
        $('.main .folder-wrap').on("scroll", function() {
          let limitTop = 50;
          if ($('.main .folder-wrap').scrollTop() > limitTop) {
            $("#btn_top").removeClass('hide');
          } else {
            $("#btn_top").addClass('hide');
          }
        });

        $("#btn_top").on('click', function() {
          $('.main .folder-wrap').animate({
            scrollTop: 0
          }, 500);
        });

        betConfirmMsg = '';
        m_sportsBetList.game_type = 0;
        m_sportsBetList.pick_list = [];
        myCash = '0';
        game_type = '3';

        sportsRequestPacket.user_id = '';
        sportsRequestPacket.sports_code = '';
        sportsRequestPacket.league_code = '';
        sportsRequestPacket.game_type = game_type;
        sportsRequestPacket.family = '0';
        sportsRequestPacket.page_start = 0;
        sportsRequestPacket.page_length = 30;
        sportsRequestPacket.search = '';

        if (sportsRequestPacket.league_code != '' || sportsRequestPacket.family != 0 || sportsRequestPacket.search != '') {
          $("#main-selector .selector .item").removeClass('active');
        }

        if (sportsRequestPacket.family == 0) {
          $(".sports-betting-wrap .topleague .sport-icon").removeClass('active');
        }

        //initBetValue('0', '0', '0', '0', '0', '0');
        initBetValue();
        initBettingCartInputComma();

        getLeagueList();
        getGameList();
        sendMsgSportsData();

        let cart = $('#betting_cart_opener');

        // $('#betting_cart_opener').draggable({
        //   scroll: false,
        //   containment: 'body', //부모 요소 안에서만 이동 범위 제한
        //   // handle: '.header' // drag 대상 안 특정 요소에 제한 (여러개 사용 가능)
        // });
        // $('.sports-betting-wrap .sports-container>.right #betting_cart').draggable({
        //   scroll: false,
        //   containment: 'body', //부모 요소 안에서만 이동 범위 제한
        //   // handle: '.header' // drag 대상 안 특정 요소에 제한 (여러개 사용 가능)
        // })
        if ($(window).width() >= 990) {
          $('#betting_cart_opener').draggable({
            scroll: false,
            containment: 'body', //limit movement range only within parent element
            // handle: '.header' // Limited to specific elements within the drag target (multiple can be used)
          });
          $('.sports-betting-wrap .sports-container>.right #betting_cart').draggable({
            scroll: false,
            containment: 'body', //limit movement range only within parent element
            // handle: '.header' // Limited to specific elements within the drag target (multiple can be used)
          });
        }
        $('#betting_cart_opener').click(function() {
          $(this).prev().toggleClass('hidden');
        })

        $('.sports-container > .right .selector > .item').click(function() {
          $(this).parent().attr('selected', $(this).data('rel'));
          $(this).parent().find('.selected').removeClass('selected')
          $(this).addClass('selected');

          if (!$(this).data('rel')) {
            //if (pg_start == 0) {
            pg_start = 0; //매번 베팅리스트 갱신
            getBettingList();
            //}
          }
        })

        initQuickBetSwitch();

        initQuickBetCash('0');

        $(".edit-button").on('click', function() {
          $(".edit-button").addClass("hide");
          $(".select-button").removeClass("hide");
          $(".quickbetting input[name=money]").attr("readonly", false);
        })
        $(".select-button").on('click', function() {
          $(".edit-button").removeClass("hide");
          $(".select-button").addClass("hide");
          $(".quickbetting input[name=money]").attr("readonly", true);
        })

        resizeIframe();
      })

      function resizeIframe() {
        if (window.innerWidth > 580) {

          $("#game_stat").css({
            'height': 580
          })
        } else {

          $("#game_stat").css({
            'height': window.innerWidth + 45
          })
        }
      }
      window.onresize = function() {
        resizeIframe();
      };

      function showSportsGameByCode(pSportsCode) {
        let classSuffix = pSportsCode;
        if (classSuffix == '') {
          classSuffix = 'all'
        }
        $(".main .split #main-selector .item").removeClass('active');
        $(".main .split #main-selector .category-" + classSuffix).addClass('active');
        $(".sports-betting-wrap .topleague .sport-icon").removeClass('active');
        $(".league-left-menu").removeClass('active');

        sportsRequestPacket.page_start = 0;
        sportsRequestPacket.sports_code = pSportsCode;
        sportsRequestPacket.family = 0;
        sportsRequestPacket.league_code = '';
        sportsRequestPacket.search = '';
        getGameList();
      }

      function onClickLeague(pSportsName, pNationCode, pLeagueCode) {
        $("#main-selector .selector .item").removeClass('active');
        $(".sports-betting-wrap .topleague .sport-icon").removeClass('active');
        $(".league-left-menu").removeClass('active');
        $(".sports-" + pSportsName + "-" + pNationCode + "-" + pLeagueCode).addClass('active');
        sportsRequestPacket.page_start = 0;
        sportsRequestPacket.league_code = pLeagueCode;
        sportsRequestPacket.sports_code = '';
        sportsRequestPacket.family = 0;
        sportsRequestPacket.search = '';
        getGameList();
        $('#left-selector').addClass('pc');
      }

      function onClickFamily(familyId) {
        if (familyId > 0) {
          $("#main-selector .selector .item").removeClass('active');
          $(".sports-betting-wrap .topleague .sport-icon").removeClass('active');
          $(".league-left-menu").removeClass('active');
          $("#league-family-" + familyId).addClass('active');
          sportsRequestPacket.page_start = 0;
          sportsRequestPacket.family = familyId;
          sportsRequestPacket.league_code = '';
          sportsRequestPacket.sports_code = '';
          sportsRequestPacket.search = '';
          getGameList();
          $('#left-selector').addClass('pc');
        }
      }

      function onClickSearch() {
        $("#main-selector .selector .item").removeClass('active');
        $(".sports-betting-wrap .topleague .sport-icon").removeClass('active');
        $(".league-left-menu").removeClass('active');
        let searchStr = $('.sports-betting-wrap .sports-container #left-selector .list .search-box input[name=search]').val();
        sportsRequestPacket.page_start = 0;
        sportsRequestPacket.league_code = '';
        sportsRequestPacket.sports_code = '';
        sportsRequestPacket.family = 0;
        sportsRequestPacket.search = searchStr;
        getGameList();
      }

      function clearSearchString() {
        $('.sports-betting-wrap .sports-container #left-selector .list .search-box input[name=search]').val('');
        onClickSearch('');
      }

      function getLeagueList() {
        var d = new Date();
        $.ajax({
          url: '//rmq.tgbet365.com/3dparty/toto_leaguelist.php?_t=' + d.getTime(),
          type: 'POST',
          dataType: 'text',
          data: {},
          success: function(jData, textStatus, jqXHR) {
	          alert(jData);
            if (jData) {
              $("#topleague").html(jData);
            } else {
              showMsg("선정된 리그가 없습니다.", "warning");
            }
          },
          error: function(jData, textStatus, errorThrown) {
            showMsg("정보 오류!", 'error');
          }
        });
      }

      function getGameList() {
        showFixtureId = -1;
        ajaxSend('//rmq.tgbet365.com/3dparty/toto_list.php', sportsRequestPacket, function(jData) {
          console.log(jData);
          if (!jData.success) {
            showMsg(jData.message || "정보 오류!", 'error');
            return;
          }

          showSportsCntInfo(jData);

          if (jData.data && jData.data.detailInfo) {
            if (jData.data.detailInfo.length == 0) {
              showMsg("진행중인 경기가 없습니다.", "warning");
            }
            rebuildSportsGameInfo(jData.data);
          } else {
            showMsg("진행중인 경기가 없습니다.", "warning");
          }
        });
      }

      function rebuildSportsGameInfo(sportsData) {
        bet_delay = sportsData.betDelay;
        if (sportsData.mapAllowMarketLimitIds) mapAllowMarketLimitIds = new Map(Object.entries(sportsData.mapAllowMarketLimitIds));
        if (sportsData.mapStrAllowMarketLimitIds) mapStrAllowMarketLimitIds = new Map(Object.entries(sportsData.mapStrAllowMarketLimitIds));
        if (sportsData.mapNotAllowMarketLimitIds) mapNotAllowMarketLimitIds = new Map(Object.entries(sportsData.mapNotAllowMarketLimitIds));
        if (sportsData.mapMarketLimitUseYn) mapMarketLimitUseYn = new Map(Object.entries(sportsData.mapMarketLimitUseYn));

        let $pagingWrapper = $('.sports-betting-wrap .paging_box');
        if (sportsData.pageStart >= sportsData.showGameCnt) {
          sportsData.pageStart = ((sportsData.showGameCnt - 1) / sportsData.pageLength | 0) * sportsData.pageLength;
        }

        let dataTwbs = $pagingWrapper.data('twbsPagination');
        if (dataTwbs) {
          $pagingWrapper.twbsPagination('destroy');
          dataTwbs.$element.twbsPagination({
            start: sportsData.pageStart,
            length: sportsData.pageLength,
            total: sportsData.showGameCnt,
          });
        } else {
          $pagingWrapper.twbsPagination({
            start: sportsData.pageStart,
            length: sportsData.pageLength,
            total: sportsData.showGameCnt,
            visiblePages: 5,
            onPageClick: function(event, start) {
              pg_start = start;
              sportsRequestPacket.page_start = start;
              getGameList();
            }
          });
        }

        let jsonDetailInfoList = sportsData.detailInfo;
        if (oldDetailInfoList.length > 0) {
          for (let i = 0; i < oldDetailInfoList.length; i++) {
            let oldDetailInfo = oldDetailInfoList[i];

            findDetailInfo = jsonDetailInfoList.find(function(element) {
              return oldDetailInfo.fixtureId == element.fixtureId;
            });

            if (findDetailInfo == null || findDetailInfo == undefined) {
              // 게임 정보 삭제
              removeGameInfo(oldDetailInfo);
              continue;
            }

            if (oldDetailInfo.rateInfo && oldDetailInfo.rateInfo.length > 0) {
              for (let j = 0; j < oldDetailInfo.rateInfo.length; j++) {
                let oldRateDetailInfo = oldDetailInfo.rateInfo[j];
                let findRateInfo = null;

                for (let k = 0; k < jsonDetailInfoList.length; k++) {
                  if (jsonDetailInfoList[k].rateInfo && jsonDetailInfoList[k].rateInfo.length > 0) {
                    findRateInfo = jsonDetailInfoList[k].rateInfo.find(function(element) {
                      return (oldRateDetailInfo.abetCode == element.abetCode &&
                        oldRateDetailInfo.dbetCode == element.dbetCode &&
                        oldRateDetailInfo.hbetCode == element.hbetCode);
                    });
                    if (findRateInfo) {
                      break;
                    }
                  }
                }

                if (findRateInfo == null || findRateInfo == undefined) {
                  // 배당정보 삭제
                  removeRateInfo(oldDetailInfo.fixtureId, oldRateDetailInfo);
                }
              }
            }
          }
          // 리그 정보 삭제
          for (let i = 0; i < oldDetailInfoList.length; i++) {
            let oldDetailInfo = oldDetailInfoList[i];
            let leagueGameCnt = getLeagueGameCnt(oldDetailInfo);
            if (leagueGameCnt <= 0) {
              $("#league-" + getLeagueHeaderStr(oldDetailInfo)).remove();
            }
          }
        }
        if (jsonDetailInfoList.length > 0) {
          let firstDetailInfo = null;
          const leagueInfoArr = [];
          const marketGroupDivNameArr = [];
          for (let i = 0; i < jsonDetailInfoList.length; i++) {
            let jsonDetailInfo = jsonDetailInfoList[i];
            let leagueHeaderInfoStr = "league-" + getLeagueHeaderStr(jsonDetailInfo);
            let arrIndex = leagueInfoArr.indexOf(leagueHeaderInfoStr);
            if (arrIndex == -1) {
              leagueInfoArr.push(leagueHeaderInfoStr);
            }
          }

          for (let i = 0; i < jsonDetailInfoList.length; i++) {
            let jsonDetailInfo = jsonDetailInfoList[i];
            let leagueHeaderInfoStr = "league-" + getLeagueHeaderStr(jsonDetailInfo);
            let prevLeagueHeaderInfoStr = '';
            let leagueHeaderObj = document.getElementById(leagueHeaderInfoStr);
            if (leagueHeaderObj == null || leagueHeaderObj == undefined) {
              // 리그추가
              let arrIndex = leagueInfoArr.indexOf(leagueHeaderInfoStr);
              if (arrIndex <= 0) {
                prevLeagueHeaderInfoStr = '';
              } else {
                prevLeagueHeaderInfoStr = leagueInfoArr[arrIndex - 1];
              }
              appendLeagueInfo(prevLeagueHeaderInfoStr, jsonDetailInfo); // 리그 헤더 추가
              appendGameInfo(jsonDetailInfo); // 게임정보 추가(기본배당 추가)
              if (firstDetailInfo == null) {
                firstDetailInfo = jsonDetailInfo;
              }

              continue;
            }

            let gameFormObjs = document.getElementsByClassName("game-" + jsonDetailInfo.fixtureId);
            if (gameFormObjs == null || gameFormObjs == undefined || gameFormObjs.length == 0) { // 새 게임 추가
              appendGameInfo(jsonDetailInfo);

              if (firstDetailInfo == null) {
                firstDetailInfo = jsonDetailInfo;
              }
              continue;
            }

            if (firstDetailInfo == null) {
              firstDetailInfo = jsonDetailInfo;
            }

            if (jsonDetailInfo.fixtureId == showFixtureId) {
              if (jsonDetailInfo.rateInfo && jsonDetailInfo.rateInfo.length > 0) {
                for (let j = 0; j < jsonDetailInfo.rateInfo.length; j++) {
                  let jsonRateDetailInfo = jsonDetailInfo.rateInfo[j];
                  // -- Continue 1X2, 12 Rate
                  if (mainMarketMap.get(jsonDetailInfo.sportsCode) == jsonRateDetailInfo.marketId && jsonRateDetailInfo.hbetCode != '') {
                    continue;
                  }

                  if (jsonRateDetailInfo.hbetCode == '') continue;

                  let marketDivName = "market-" + jsonDetailInfo.fixtureId + "-" + jsonRateDetailInfo.marketId;
                  let arrIndex = marketGroupDivNameArr.indexOf(marketDivName);
                  if (arrIndex == -1) {
                    marketGroupDivNameArr.push(marketDivName);
                  }
                }
                for (let j = 0; j < jsonDetailInfo.rateInfo.length; j++) {
                  let jsonRateDetailInfo = jsonDetailInfo.rateInfo[j];
                  // -- Continue 1X2, 12 Rate
                  if (mainMarketMap.get(jsonDetailInfo.sportsCode) == jsonRateDetailInfo.marketId && jsonRateDetailInfo.hbetCode != '') {
                    continue;
                  }

                  if (jsonRateDetailInfo.hbetCode == '') continue;

                  let betInfoClassName = "bet-info-" + getBetKeyStr(jsonDetailInfo.fixtureId, jsonRateDetailInfo);
                  let betLiObjs = document.getElementsByClassName(betInfoClassName);
                  let prevMarketDivName = '';
                  if (betLiObjs == null || betLiObjs == undefined || betLiObjs.length == 0) {
                    // -- Check Market And Add Market
                    let marketDivName = "market-" + jsonDetailInfo.fixtureId + "-" + jsonRateDetailInfo.marketId;
                    let marketDivObjs = document.getElementsByClassName(marketDivName)
                    if (marketDivObjs == null || marketDivObjs == undefined || marketDivObjs.length == 0) {
                      let arrIndex = marketGroupDivNameArr.indexOf(marketDivName);
                      if (arrIndex <= 0) {
                        prevMarketDivName = '';
                      } else {
                        prevMarketDivName = marketGroupDivNameArr[arrIndex - 1];
                      }
                      let $marketSel = getAppendMarketDivInfo(jsonDetailInfo.fixtureId, jsonRateDetailInfo, getSportsImgPath(jsonDetailInfo));
                      if (prevMarketDivName == '') {
                        $(".detail-wrap .reverse .option-group-wrap").prepend($marketSel);
                      } else {
                        $($marketSel).insertAfter($("." + prevMarketDivName));
                      }
                    }

                    let $sel = getAppendRateInfo(jsonDetailInfo, j);
                    let insertAfterDivName = '';
                    let k = j - 1;
                    while (1) {
                      if (k < 0) break;
                      if (jsonRateDetailInfo.marketId == jsonDetailInfo.rateInfo[k].marketId) {
                        insertAfterDivName = "bet-info-" + getBetKeyStr(jsonDetailInfo.fixtureId, jsonDetailInfo.rateInfo[k]);
                        break;
                      }
                      k--;
                    }

                    if (insertAfterDivName == '') {
                      if (jsonRateDetailInfo.family == '11' ||
                        jsonRateDetailInfo.family == '12' ||
                        jsonRateDetailInfo.family == '47' ||
                        jsonRateDetailInfo.family == '120') {
                        $(".detail-wrap .reverse .option-group-wrap ." + marketDivName + " .option-bunch .option-wrap").prepend($sel);
                      } else {
                        $(".detail-wrap .reverse .option-group-wrap ." + marketDivName + " .option-bunch").prepend($sel);
                      }
                    } else {
                      $($sel).insertAfter($("." + insertAfterDivName));
                    }
                  }
                  // -- Update Rate Info
                  changeRateValue(jsonDetailInfo.fixtureId, jsonRateDetailInfo, false, jsonDetailInfo.groupCnt, '', '', '', '', '', '');
                }
              }
              $(".tracker").html(buildTrackerInfo(jsonDetailInfo));
            }
            // 기본 배당 업데이트
            if (jsonDetailInfo.rateInfo && jsonDetailInfo.rateInfo.length > 0) {
              for (let j = 0; j < jsonDetailInfo.rateInfo.length; j++) {
                let jsonRateDetailInfo = jsonDetailInfo.rateInfo[j];

                if (mainMarketMap.get(jsonDetailInfo.sportsCode) == jsonRateDetailInfo.marketId && jsonRateDetailInfo.hbetCode != '') {

                  let kind = $(".game-" + jsonDetailInfo.fixtureId).attr("data-kind");
                  if (kind == 1) {
                    //기본배당이 빈칸
                    $sel = getMainGameBetInfo(jsonDetailInfo, jsonRateDetailInfo);
                    $(".game-" + jsonDetailInfo.fixtureId).replaceWith($sel);
                  } else {
                    changeRateValue(jsonDetailInfo.fixtureId, jsonRateDetailInfo, true, jsonDetailInfo.groupCnt, jsonDetailInfo.homeScore, jsonDetailInfo.awayScore, jsonDetailInfo.homeScoreSub, jsonDetailInfo.awayScoreSub, jsonDetailInfo.period, jsonDetailInfo.playTime);
                  }
                }
              }
            }
          }
          if (showFixtureId == -1) {
            $("#market-list .detail-wrap").remove();
            if (firstDetailInfo != null) {
              buildRateDetailInfo(firstDetailInfo);
              showFixtureId = firstDetailInfo.fixtureId;
              $(".main-odds").removeClass("active");
              $(".game-" + showFixtureId).addClass("active");
            }
          }
        } else {
          showFixtureId = -1;
          $("#market-list .detail-wrap").remove();
        }
        oldDetailInfoList = jsonDetailInfoList;
        setBetPickOnAll();
        showBetPickStatus();
      }

      function getLeagueHeaderStr(detailInfo) {
        return detailInfo.leagueCode + "-" + detailInfo.startDateMod;
      }

      function getLeagueGameCnt(detailInfo) {
        let leagueHeaderInfoStr = getLeagueHeaderStr(detailInfo);
        let gameWrapObj = document.getElementById("game-wrap-" + leagueHeaderInfoStr);
        if (gameWrapObj == undefined && gameWrapObj == null) {
          return 0;
        }

        return gameWrapObj.children.length;
      }

      function getBetKeyStr(fixtureId, rateDetailInfo) {
        let str = fixtureId + "-" + (rateDetailInfo.abetCode || '') + "-" + (rateDetailInfo.dbetCode || '') + "-" + (rateDetailInfo.hbetCode || '');
        return str;
      }

      // 게임 정보 삭제
      function removeGameInfo(detailInfo) {
        if (detailInfo.fixtureId == showFixtureId) {
          showFixtureId = -1;
          $("#market-list .detail-wrap").remove();
        }
        $(".game-" + detailInfo.fixtureId).remove();
        $(".mob-game-" + detailInfo.fixtureId + "-market-wrap").remove();
      }

      function removeRateInfo(fixtureId, rateDetailInfo) {
        if (fixtureId == showFixtureId) {
          $(".bet-info-" + getBetKeyStr(fixtureId, rateDetailInfo)).remove();
          let oddsLength = $('.market-' + fixtureId + "-" + rateDetailInfo.marketId + ' .bet').length;
          if (oddsLength == 0) {
            $('.market-' + fixtureId + "-" + rateDetailInfo.marketId).remove();
          }
        }
      }

      function getSportsImgPath(detailInfo) {
        let sportsImgPath = detailInfo.sportsImgPath;

        return sportsImgPath;
      }

      function appendLeagueInfo(prevLeagueHeaderInfoStr, detailInfo) {
        let curLeagueHeaderStr = getLeagueHeaderStr(detailInfo);
        let startTimeStr = detailInfo.startDate.substr(5);
        let sportsImgPath = getSportsImgPath(detailInfo);

        let $sel = `
           <div class="folder" id="league-${curLeagueHeaderStr}">
            <div class="accordion-opened" data-role="accordion:switcher">
              <div class="left sport-icon" data-sport="${detailInfo.sportsName}">
                <div class="sports-logo">
                  <img src="${sportsImgPath}" alt="">
                </div>
                <img src="/win/images/sports/arrows_1.svg" width="10" height="11" alt="">
                <div class="flag-icon">
                  <img src="${detailInfo.leagueImgPath}" alt="">
                </div>
                <div class="location">${detailInfo.nationName}</div>
                <img src="/win/images/sports/arrows_1.svg" width="10" height="11" alt="">
                <div class="league">${detailInfo.leagueName}</div>
              </div>
              <div class="right">
                <div class="time mob">${startTimeStr}</div>
                <i class="icon-iconARupA"></i>
              </div>
            </div>
            <div data-role="accordion:switchee" id="game-wrap-${curLeagueHeaderStr}">
      
            </div>
          </div>
          `;
        if (prevLeagueHeaderInfoStr == '') {
          $($sel).insertAfter("#fixture-list #game-folders #top-hidden");
        } else {
          $($sel).insertAfter("#" + prevLeagueHeaderInfoStr);
        }
      }

      function getMainGameBetInfo(detailInfo, rateDetailInfo) {
        let $sel = '';
        let startTimeStr = detailInfo.startDate.substr(5);
        let rateCnt = detailInfo.groupCnt;
        if (rateCnt < 0) rateCnt = 0;
        let betKey = getBetKeyStr(detailInfo.fixtureId, rateDetailInfo);
        let homeDisableStr = '';
        let awayDisableStr = '';
        let drawDisableStr = '';
        if (rateDetailInfo.rateHomeStatus != 1) {
          homeDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 0);
        }
        if (rateDetailInfo.rateAwayStatus != 1) {
          awayDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 0);
        }
        if (rateDetailInfo.rateDrawStatus != 1) {
          drawDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 0);
        }
        let pickTitle = detailInfo.homeTeamName + " vs " + detailInfo.awayTeamName;
        let pickDescHome = detailInfo.homeTeamName;
        let pickDescDraw = '무승부';
        let pickDescAway = detailInfo.awayTeamName;

        if (rateDetailInfo.family == '1') {
          $sel = `
            <div class="row game-${detailInfo.fixtureId} bet-info-${betKey} main-odds" id="rate-topic-${detailInfo.fixtureId}" data-kind="0">
              <div class="top">
                <div class="left">
                  <div class="status"> 상태: <span class="period-${detailInfo.fixtureId}">${detailInfo.period} [${detailInfo.homeScoreSub}:${detailInfo.awayScoreSub}]</span> </div>
                </div>
                <div class="right">
                  <div data-role="drawer" class="pc" onclick="showRateDetail(${detailInfo.fixtureId})">+<span class="cnt-${detailInfo.fixtureId}">${rateCnt}</span>&nbsp 추가 베팅옵션</div>
                  <div data-role="drawer" class="mob" onclick="showToggleRateDetail(${detailInfo.fixtureId})">+<span class="cnt-${detailInfo.fixtureId}">${rateCnt}</span>&nbsp 추가 베팅옵션</div>
                </div>
              </div>
              <div class="middle">
                <div class="time">${startTimeStr}</div>
                <div class="score-wrap">
                  <div class="left">
                    <div class="team-logo">
                      <img loading="lazy" src="${detailInfo.homeTeamImgPath}" alt="">
                    </div>
                    <div class="score homescore-${detailInfo.fixtureId}"> ${detailInfo.homeScore} </div>
                  </div>
                  <div class="vs"> VS </div>
                  <div class="right">
                    <div class="score awayscore-${detailInfo.fixtureId}"> ${detailInfo.awayScore} </div>
                    <div class="team-logo">
                      <img loading="lazy" src="${detailInfo.awayTeamImgPath}" alt="">
                    </div>
                  </div>
                </div>
                <div class="time playtime-${detailInfo.fixtureId}">${detailInfo.playTime}</div>
              </div>
              <div class="bet-wrap">
                <div class="bet odds-${rateDetailInfo.hbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.hbetCode}', ${rateDetailInfo.hrate}, '', 0, '${pickDescHome}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
                  <div class="left">
                    <div class="name">${detailInfo.homeTeamName}</div>
                  </div>
                  <div class="odds-wrap right bet-pick home-pick ${homeDisableStr}">
                    <div class="odds">${parseFloat(rateDetailInfo.hrate).toFixed(2)}</div>
                  </div>
                </div>
                <div class="bet draw-bet odds-${rateDetailInfo.dbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.dbetCode}', ${rateDetailInfo.drate}, '', 2, '${pickDescDraw}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
                  <div class="name" hidden="">무승부</div>
                  <div class="odds-wrap short bet-pick draw-pick ${drawDisableStr}">
                    <div class="odds">${parseFloat(rateDetailInfo.drate).toFixed(2)}</div>
                  </div>
                </div>
                <div class="bet odds-${rateDetailInfo.abetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.abetCode}', ${rateDetailInfo.arate}, '', 1, '${pickDescAway}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
                  <div class="odds-wrap left bet-pick away-pick ${awayDisableStr}">
                    <div class="odds">${parseFloat(rateDetailInfo.arate).toFixed(2)}</div>
                  </div>
                  <div class="right">
                    <div class="name">${detailInfo.awayTeamName}</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="mob-market-list mob-game-${detailInfo.fixtureId}-market-wrap mob">
            </div>
            `;
        } else {
          $sel = `
            <div class="row game-${detailInfo.fixtureId} bet-info-${betKey} main-odds" id="rate-topic-${detailInfo.fixtureId}" data-kind="0">
              <div class="top">
                <div class="left">
                  <div class="status"> 상태: <span class="period-${detailInfo.fixtureId}">${detailInfo.period} [${detailInfo.homeScoreSub}:${detailInfo.awayScoreSub}]</span> </div>
                </div>
                <div class="right">
                  <div data-role="drawer" class="pc" onclick="showRateDetail(${detailInfo.fixtureId})">+<span class="cnt-${detailInfo.fixtureId}">${rateCnt}</span>&nbsp 추가 베팅옵션</div>
                  <div data-role="drawer" class="mob" onclick="showToggleRateDetail(${detailInfo.fixtureId})">+<span class="cnt-${detailInfo.fixtureId}">${rateCnt}</span>&nbsp 추가 베팅옵션</div>
                </div>
              </div>
              <div class="middle">
                <div class="time">${startTimeStr}</div>
                <div class="score-wrap">
                  <div class="left">
                    <div class="team-logo">
                      <img loading="lazy" src="${detailInfo.homeTeamImgPath}" alt="">
                    </div>
                    <div class="score homescore-${detailInfo.fixtureId}"> ${detailInfo.homeScore} </div>
                  </div>
                  <div class="vs"> VS </div>
                  <div class="right">
                    <div class="score awayscore-${detailInfo.fixtureId}"> ${detailInfo.awayScore} </div>
                    <div class="team-logo">
                      <img loading="lazy" src="${detailInfo.awayTeamImgPath}" alt="">
                    </div>
                  </div>
                </div>
                <div class="time playtime-${detailInfo.fixtureId}">${detailInfo.playTime}</div>
              </div>
              <div class="bet-wrap">
                <div class="bet odds-${rateDetailInfo.hbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.hbetCode}', ${rateDetailInfo.hrate}, '', 0, '${pickDescHome}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
                  <div class="left">
                    <div class="name">${detailInfo.homeTeamName}</div>
                  </div>
                  <div class="odds-wrap right bet-pick home-pick ${homeDisableStr}">
                    <div class="odds">${parseFloat(rateDetailInfo.hrate).toFixed(2)}</div>
                  </div>
                </div>
                <div class="bet draw-bet" data-bet-id="-" data-bet-status="-" data-odds="-">
                  <div class="name" hidden="">무승부</div>
                  <div class="odds-wrap short icon-iconLock">
                    <div class="odds">VS</div>
                  </div>
                </div>
                <div class="bet odds-${rateDetailInfo.abetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.abetCode}', ${rateDetailInfo.arate}, '', 1, '${pickDescAway}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
                  <div class="odds-wrap left bet-pick away-pick ${awayDisableStr}">
                    <div class="odds">${parseFloat(rateDetailInfo.arate).toFixed(2)}</div>
                  </div>
                  <div class="right">
                    <div class="name">${detailInfo.awayTeamName}</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="mob-market-list mob-game-${detailInfo.fixtureId}-market-wrap mob">
            </div>
            `;
        }
        return $sel;
      }

      function getMainGameEmptyBetInfo(detailInfo) {
        let $sel = '';
        let startTimeStr = detailInfo.startDate.substr(5);
        let rateCnt = detailInfo.groupCnt + 1;
        if (rateCnt < 0) rateCnt = 0;

        $sel = `
            <div class="row game-${detailInfo.fixtureId} main-odds" id="rate-topic-${detailInfo.fixtureId}" data-kind="1">
              <div class="top">
                <div class="left">
                  <div class="status"> 상태: <span class="period-${detailInfo.fixtureId}">${detailInfo.period} [${detailInfo.homeScoreSub}:${detailInfo.awayScoreSub}]</span> </div>
                </div>
                <div class="right">
                  <div data-role="drawer" class="pc" onclick="showRateDetail(${detailInfo.fixtureId})">+<span class="cnt-${detailInfo.fixtureId}">${rateCnt}</span>&nbsp 추가 베팅옵션</div>
                  <div data-role="drawer" class="mob" onclick="showToggleRateDetail(${detailInfo.fixtureId})">+<span class="cnt-${detailInfo.fixtureId}">${rateCnt}</span>&nbsp 추가 베팅옵션</div>
                </div>
              </div>
              <div class="middle">
                <div class="time">${startTimeStr}</div>
                <div class="score-wrap">
                  <div class="left">
                    <div class="team-logo">
                      <img loading="lazy" src="${detailInfo.homeTeamImgPath}" alt="">
                    </div>
                    <div class="score homescore-${detailInfo.fixtureId}"> ${detailInfo.homeScore} </div>
                  </div>
                  <div class="vs"> VS </div>
                  <div class="right">
                    <div class="score awayscore-${detailInfo.fixtureId}"> ${detailInfo.awayScore} </div>
                    <div class="team-logo">
                      <img loading="lazy" src="${detailInfo.awayTeamImgPath}" alt="">
                    </div>
                  </div>
                </div>
                <div class="time playtime-${detailInfo.fixtureId}">${detailInfo.playTime}</div>
              </div>
              <div class="bet-wrap">
                <div class="bet" data-role="bet" >
                  <div class="left">
                    <div class="name">${detailInfo.homeTeamName}</div>
                  </div>
                  <div class="odds-wrap right icon-iconLock">
                    <div class="odds">0</div>
                  </div>
                </div>
                <div class="bet draw-bet" data-bet-id="-" data-bet-status="-" data-odds="-">
                  <div class="name" hidden="">무승부</div>
                  <div class="odds-wrap short icon-iconLock">
                    <div class="odds">VS</div>
                  </div>
                </div>
                <div class="bet" data-role="bet" >
                  <div class="odds-wrap left icon-iconLock">
                    <div class="odds">0</div>
                  </div>
                  <div class="right">
                    <div class="name">${detailInfo.awayTeamName}</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="mob-market-list mob-game-${detailInfo.fixtureId}-market-wrap mob">
            </div>
            `;
        return $sel;
      }

      function appendGameInfo(detailInfo) {
        let leagueHeaderInfoStr = getLeagueHeaderStr(detailInfo);
        let findRateDetailInfo = null;
        if (detailInfo.rateInfo) {
          for (let i = 0; i < detailInfo.rateInfo.length; i++) {
            let rateDetailInfo = detailInfo.rateInfo[i];

            if (mainMarketMap.get(detailInfo.sportsCode) == rateDetailInfo.marketId && rateDetailInfo.hbetCode != '') {
              findRateDetailInfo = rateDetailInfo;
              break;
            }
          }
        }

        let $sel = '';
        if (findRateDetailInfo) {
          $sel = getMainGameBetInfo(detailInfo, findRateDetailInfo);
        } else {
          $sel = getMainGameEmptyBetInfo(detailInfo);
        }
        $("#league-" + leagueHeaderInfoStr + " #game-wrap-" + leagueHeaderInfoStr).append($sel);
      }

      function changeRateValue(fixtureId, rateDetailInfo, mainRateYn, rateCnt, homeScore, awayScore, homeScoreSub, awayScoreSub, period, playTime) {
        let oddsClassName = '';
        let oddsWrapClassName = '';
        let isDisable = false;
        if (rateDetailInfo.hbetCode && rateDetailInfo.hbetCode.length > 0) {
          oddsClassName = ".odds-" + rateDetailInfo.hbetCode + " .odds-wrap .odds";
          oddsWrapClassName = ".odds-" + rateDetailInfo.hbetCode + " .odds-wrap";
          let oldRateVal = parseFloat($(oddsClassName).html()).toFixed(2);
          let newRateVal = parseFloat(rateDetailInfo.hrate).toFixed(2);
          if (oldRateVal != newRateVal) {
            if (parseFloat(oldRateVal) < parseFloat(newRateVal)) {
              $(oddsWrapClassName).addClass("odds-up");
            } else {
              $(oddsWrapClassName).addClass("odds-down");
            }
            $(oddsClassName).html(newRateVal);
          } else {
            $(oddsWrapClassName).removeClass("odds-up");
            $(oddsWrapClassName).removeClass("odds-down");
          }
        }
        if (rateDetailInfo.abetCode && rateDetailInfo.abetCode.length > 0) {
          oddsClassName = ".odds-" + rateDetailInfo.abetCode + " .odds-wrap .odds";
          oddsWrapClassName = ".odds-" + rateDetailInfo.abetCode + " .odds-wrap";
          let oldRateVal = parseFloat($(oddsClassName).html()).toFixed(2);
          let newRateVal = parseFloat(rateDetailInfo.arate).toFixed(2);

          if (oldRateVal != newRateVal) {
            if (parseFloat(oldRateVal) < parseFloat(newRateVal)) {
              $(oddsWrapClassName).addClass("odds-up");
            } else {
              $(oddsWrapClassName).addClass("odds-down");
            }
            $(oddsClassName).html(newRateVal);
          } else {
            $(oddsWrapClassName).removeClass("odds-up");
            $(oddsWrapClassName).removeClass("odds-down");
          }
        }
        if (rateDetailInfo.dbetCode && rateDetailInfo.abetCode.length > 0) {
          oddsClassName = ".odds-" + rateDetailInfo.dbetCode + " .odds-wrap .odds";
          oddsWrapClassName = ".odds-" + rateDetailInfo.dbetCode + " .odds-wrap";
          let oldRateVal = parseFloat($(oddsClassName).html()).toFixed(2);
          let newRateVal = parseFloat(rateDetailInfo.drate).toFixed(2);
          if (oldRateVal != newRateVal) {
            if (parseFloat(oldRateVal) < parseFloat(newRateVal)) {
              $(oddsWrapClassName).addClass("odds-up");
            } else {
              $(oddsWrapClassName).addClass("odds-down");
            }
            $(oddsClassName).html(newRateVal);
          } else {
            $(oddsWrapClassName).removeClass("odds-up");
            $(oddsWrapClassName).removeClass("odds-down");
          }
        }

        // -- Change Rate Status
        let key = getBetKeyStr(fixtureId, rateDetailInfo);
        if (rateDetailInfo.rateHomeStatus != 1) {
          $(".bet-info-" + key + " .odds-wrap.bet-pick.home-pick").addClass("icon-iconLock");
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 2);
        } else {
          $(".bet-info-" + key + " .odds-wrap.bet-pick.home-pick").removeClass("icon-iconLock");
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 0);
        }
        if (rateDetailInfo.rateAwayStatus != 1) {
          $(".bet-info-" + key + " .odds-wrap.bet-pick.away-pick").addClass("icon-iconLock");
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 2);
        } else {
          $(".bet-info-" + key + " .odds-wrap.bet-pick.away-pick").removeClass("icon-iconLock");
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 0);
        }
        if (rateDetailInfo.rateDrawStatus != 1) {
          $(".bet-info-" + key + " .odds-wrap.bet-pick.draw-pick").addClass("icon-iconLock");
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 2);
        } else {
          $(".bet-info-" + key + " .odds-wrap.bet-pick.draw-pick").removeClass("icon-iconLock");
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 0);
        }

        if (mainRateYn == true) {
          if (rateCnt < 0) rateCnt = 0;
          $(".game-" + fixtureId + " .cnt-" + fixtureId).html(rateCnt);
          $(".game-" + fixtureId + " .homescore-" + fixtureId).html(homeScore);
          $(".game-" + fixtureId + " .awayscore-" + fixtureId).html(awayScore);
          $(".game-" + fixtureId + " .period-" + fixtureId).html(period + " [" + homeScoreSub + ":" + awayScoreSub + "]");
          $(".game-" + fixtureId + " .playtime-" + fixtureId).html(playTime);
        }
      }

      function getAppendMarketDivInfo(fixtureId, rateDetailInfo, sportsImgPath) {
        let curKey = "market-" + fixtureId + "-" + rateDetailInfo.marketId;
        $sel = `<div class="option-group ${curKey} market-family-${rateDetailInfo.family}" data-market-id="${rateDetailInfo.marketId}">
                <div class="accordion-opened" data-role="accordion:switcher">
                  <div class="left">
                    <div class="sport-icon">
                      <div class="sports-logo">
                        <img src="${sportsImgPath}" alt="">
                      </div>
                      <div class="title"> ${rateDetailInfo.marketName} </div>
                    </div>
                  </div>
                  <div class="right">
                    <i class="icon-iconARupA"></i>
                  </div>
                </div>
                <div data-role="accordion:switchee" data-market-name="${rateDetailInfo.marketName}">
                  <div class="option-bunch">
                    `;
        if (rateDetailInfo.family == '11' ||
          rateDetailInfo.family == '12' ||
          rateDetailInfo.family == '47' ||
          rateDetailInfo.family == '120' ||
          rateDetailInfo.family == '13' ||
          rateDetailInfo.family == '29'
        ) {
          $sel += `
                    <div>
                      <div class="option-wrap">
                      </div>
                    </div>
              `;
        }
        $sel += `   </div>
                </div>
              </div>
          `;
        return $sel;
      }

      function buildTrackerInfo(detailInfo) {
        let $sel = '';
        let periods = [];
        let period = 0;
        let height = 580;
        let homeScores = detailInfo.homeScores.split("|");
        let awayScores = detailInfo.awayScores.split("|");
        if (detailInfo.sportsCode == '6046') { //축구
          height = 625;
          period = 5;
          periods = ['전반', '후반', '연장 전반', '연장 후반', 'PK'];
        } else if (detailInfo.sportsCode == '48242' || detailInfo.sportsCode == '131506') { //농구,풋볼
          height = 625;
          //if (period==)
          period = 5;
          periods = ['1', '2', '3', '4', 'ET'];
        } else if (detailInfo.sportsCode == '154830') { //테니스
          height = 595;
          period = 5;
          periods = ['1', '2', '3', '4', 'ET'];
        } else if (detailInfo.sportsCode == '35232') { //하키
          height = 595;
          period = 4;
          periods = ['1', '2', '3', 'ET'];
        } else if (detailInfo.sportsCode == '154914') { //야구
          height = 595;
          period = 10;
          periods = ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'ET'];
        }

        if (window.innerWidth <= 580) {
          height = window.innerWidth
        }

        if (period) {
          $sel = `
            <div class="market-score">
              <!--div class="score-head"></div-->
              <div class="score-body">
                <div class="score-board">
                  <span class="score-title">${detailInfo.period}</span>
                  <div class="team-score">
                    <em><span>${detailInfo.homeTeamName}</span></em>
                    <strong>${detailInfo.homeScore} : ${detailInfo.awayScore}</strong>
                    <em><span>${detailInfo.awayTeamName}</span></em>
                  </div>
                  <div class="score-point">
                    <div class="title-match">
                      <div>
                        <img src="${detailInfo.sportsImgPath}" class="sport-sort" alt="">
                        <img src="${detailInfo.leagueImgPath}" class="country" alt="">
                        <span class="game-title">${detailInfo.nationName} / ${detailInfo.leagueName}</span>
                      </div>
                    </div>
                    <div class="game-result">
                      <table class="table-check-point-1">
                        <thead>
                          <tr>
                            <th></th>
            `;
          for (let i = 0; i < period; i++) {
            $sel += `<th>${periods[i]}</th>`;
          }
          $sel += `
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              <div class="team-info">
                                <div class="team-logo">
                                  <img src="${detailInfo.homeTeamImgPath}" alt="" >
                                </div>
                                <span class="team-name">${detailInfo.homeTeamName}</span>
                              </div>
                            </td>
            `;
          for (let i = 0; i < period; i++) {
            if (!homeScores[i + 1]) homeScores[i + 1] = '';
            $sel += `<td>${homeScores[i+1]}</td>`;
          }
          $sel += `
                            <td>${detailInfo.homeScore}</td>
                          </tr>
                          <tr>
                            <td>
                              <div class="team-info">
                                <div class="team-logo">
                                  <img src="${detailInfo.awayTeamImgPath}" alt="" >
                                </div>
                                <span class="team-name">${detailInfo.awayTeamName}</span>
                              </div>
                            </td>
            `;
          for (let i = 0; i < period; i++) {
            if (!awayScores[i + 1]) awayScores[i + 1] = '';
            $sel += `<td>${awayScores[i+1]}</td>`;
          }
          $sel += `
                            <td>${detailInfo.awayScore}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="overlay"></div>
                <img src="/win/images/sports/tr_${detailInfo.sportsCode}.png" alt="">
              </div>
            </div>
            `;
        }
        return $sel;
      }

      function buildRateDetailInfo(detailInfo) {
        let $sel = `
          <div class="detail-wrap">
            <div class="accordion-opened pc" data-role="accordion:switcher">
              <div class="left">
                <div> ${detailInfo.nationName} </div>
                <div class="title"> 
                  <img src="/win/images/sports/arrows_1.svg" alt="" width="10" height="11">
                  <div class="home team-name">${detailInfo.homeTeamName}</div>
                  <div class="vs">vs</div>
                  <div class="away team-name">${detailInfo.awayTeamName}</div>
                </div>
              </div>
              <div class="right"> <span class="active"></span> <i class="icon-iconARupA"></i> </div>
            </div>
            <div data-role="accordion:switchee" data-tracker-id="">
              <div class="tracker">
          `;
        $sel += buildTrackerInfo(detailInfo); //<iframe id="game_stat" scrolling="no" frameborder="0" src="" width="100%" height="${height}"></iframe>
        $sel += `
              </div>
            </div>
            <div class="reverse">
              <div class="option-group-wrap" data-fixture-id="${detailInfo.fixtureId}" data-bet-left="${detailInfo.homeTeamName}" data-bet-right="${detailInfo.awayTeamName}">
      
              </div>
              <div class="type-wrap">
                <div class="type" data-type="" onclick="filterMarket('')" selected>전체</div>
                <div class="type" data-type="언더오버" onclick="filterMarket('언더오버')">언더/오버</div>
                <div class="type" data-type="핸디캡" onclick="filterMarket('핸디캡')">핸디캡</div>
                <div class="type" data-type="기타" onclick="filterMarket('기타')">기타</div>
              </div>
            </div>
          </div>
          `;
        $("#market-list").html($sel);

        $(".mob-market-list").html('');
        $(".mob-game-" + detailInfo.fixtureId + "-market-wrap").html($sel);

        buildRateDetailContent(detailInfo);
        resizeIframe();
      }

      function buildRateDetailContent(detailInfo) {
        for (let i = 0; i < detailInfo.rateInfo.length; i++) {
          let rateDetailInfo = detailInfo.rateInfo[i];
          // 승무패 정보는 제외한다..
          if (mainMarketMap.get(detailInfo.sportsCode) == rateDetailInfo.marketId && rateDetailInfo.hbetCode != '') {
            continue;
          }

          if (rateDetailInfo.hbetCode == '') continue;

          let marketDivName = "market-" + detailInfo.fixtureId + "-" + rateDetailInfo.marketId;
          let marketDivObjs = document.getElementsByClassName(marketDivName)
          if (marketDivObjs == null || marketDivObjs == undefined || marketDivObjs.length == 0) {
            let $marketSel = getAppendMarketDivInfo(detailInfo.fixtureId, rateDetailInfo, getSportsImgPath(detailInfo));
            $(".detail-wrap .reverse .option-group-wrap").append($marketSel);
          }
          let $sel = getAppendRateInfo(detailInfo, i);

          if (rateDetailInfo.family == '11' ||
            rateDetailInfo.family == '12' ||
            rateDetailInfo.family == '47' ||
            rateDetailInfo.family == '120' ||
            rateDetailInfo.family == '13' ||
            rateDetailInfo.family == '29'
          ) {
            $(".detail-wrap .reverse .option-group-wrap ." + marketDivName + " .option-bunch .option-wrap").append($sel);
          } else {
            $(".detail-wrap .reverse .option-group-wrap ." + marketDivName + " .option-bunch").append($sel);
          }
        }
      }

      function showRateDetail(fixtureId) {
        $("#market-list .detail-wrap").remove();
        let oldDetailInfo = null;

        for (let i = 0; i < oldDetailInfoList.length; i++) {
          if (fixtureId == oldDetailInfoList[i].fixtureId) {
            oldDetailInfo = oldDetailInfoList[i];
            showFixtureId = fixtureId;
            break;
          }
        }
        if (oldDetailInfo != null) {
          $(".main-odds").removeClass("active");
          $(".game-" + fixtureId).addClass("active");

          buildRateDetailInfo(oldDetailInfo);

          setBetPickOnAll();
        }
      }

      function showToggleRateDetail(fixtureId) {
        if ($(".game-" + fixtureId).hasClass("active")) {
          $(".game-" + fixtureId).removeClass("active");
          $(".mob-game-" + fixtureId + "-market-wrap").html("");
        } else {
          showRateDetail(fixtureId);

          let elementId = "rate-topic-" + fixtureId;
          let topicObj = document.getElementById(elementId);
          if (topicObj) {
            let offset = parseInt(topicObj.offsetTop - topicObj.offsetHeight + 42);
            if (offset > 0) {
              document.getElementById("odds-folder-wrap").scrollTop = offset;
            }
          }

        }
      }

      function getAppendRateInfo(detailInfo, i) {
        $sel = '';
        let rateDetailInfo = detailInfo.rateInfo[i];
        if (rateDetailInfo.family == '1') { //승무패
          $sel = getRateInfo_1x2(detailInfo, i);
        }
        if (rateDetailInfo.family == '2') { //승패
          $sel = getRateInfo_12(detailInfo, i);
        }
        if (rateDetailInfo.family == '7') { //언더오버
          $sel = getRateInfo_under_over(detailInfo, i);
        }
        if (rateDetailInfo.family == '8') { //아시안핸디캡
          $sel = getRateInfo_handi(detailInfo, i);
        }
        if (rateDetailInfo.family == '9') { //E-Sports 핸디캡
          $sel = getRateInfo_handi(detailInfo, i);
        }
        if (rateDetailInfo.family == '10') { //홀짝
          $sel = getRateInfo_odd_even(detailInfo, i);
        }
        if (rateDetailInfo.family == '11') { //정확한스코어
          $sel = getRateInfo_correct_score(detailInfo, i);
        }
        if (rateDetailInfo.family == '12') { //double chance
          $sel = getRateInfo_double_chance(detailInfo, i);
        }
        if (rateDetailInfo.family == '16') { //YesNo
          $sel = getRateInfo_yes_no(detailInfo, i);
        }
        if (rateDetailInfo.family == '47' //승무패 및 언더오버
          ||
          rateDetailInfo.family == '120' //승패 및 언더오버
        ) {
          $sel = getRateInfo_1x2_under_over(detailInfo, i);
        }
        if (rateDetailInfo.family == '13') { // 전반전/풀타임
          $sel = getRateInfo_ht_ft(detailInfo, i);
        }
        if (rateDetailInfo.family == '29') { // 득점수
          $sel = getRateInfo_number_of_goals(detailInfo, i);
        }
        if (rateDetailInfo.family == '19') { // 정확한 언/오
          $sel = getRateInfo_exactly_under_over(detailInfo, i);
        }
        if (rateDetailInfo.family == '22') { // 다음득점
          $sel = getRateInfo_next_goal(detailInfo, i);
        }
        if (rateDetailInfo.family == '14') { // 유럽핸디캡
          $sel = getRateInfo_european_handicap(detailInfo, i);
        }
        return $sel;
      }

      // 승무패
      function getRateInfo_1x2(detailInfo, i) {
        let rateDetailInfo = detailInfo.rateInfo[i];
        let betKey = getBetKeyStr(detailInfo.fixtureId, rateDetailInfo);
        let homeDisableStr = '';
        let awayDisableStr = '';
        let drawDisableStr = '';
        if (rateDetailInfo.rateHomeStatus != 1) {
          homeDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 0);
        }
        if (rateDetailInfo.rateAwayStatus != 1) {
          awayDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 0);
        }
        if (rateDetailInfo.rateDrawStatus != 1) {
          drawDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 0);
        }
        let pickTitle = detailInfo.homeTeamName + " vs " + detailInfo.awayTeamName;
        let pickDescHome = detailInfo.homeTeamName;
        let pickDescDraw = '무승부';
        let pickDescAway = detailInfo.awayTeamName;
        let $sel = `
          <div class="bet-info-${betKey}">
            <div class="option-wrap">
              <div class="option bet odds-${rateDetailInfo.hbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.hbetCode}', ${rateDetailInfo.hrate}, '', 0, '${pickDescHome}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
                <div class="left">
                  <div class="name">${detailInfo.homeTeamName} </div>
                </div>
                <div class="right">
                  <div class="odds-wrap bet-pick home-pick ${homeDisableStr}">
                    <div class="right odds ">${parseFloat(rateDetailInfo.hrate).toFixed(2)}</div>
                  </div>
                </div>
              </div>
              <div class="option bet odds-${rateDetailInfo.dbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.dbetCode}', ${rateDetailInfo.drate}, '', 2, '${pickDescDraw}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
                <div class="left">
                  <div class="name">무승부 </div>
                </div>
                <div class="right">
                  <div class="odds-wrap bet-pick draw-pick ${drawDisableStr}">
                    <div class="right odds ">${parseFloat(rateDetailInfo.drate).toFixed(2)}</div>
                  </div>
                </div>
              </div>
              <div class="option bet odds-${rateDetailInfo.abetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.abetCode}', ${rateDetailInfo.arate}, '', 1, '${pickDescAway}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
                <div class="left">
                  <div class="name">${detailInfo.awayTeamName} </div>
                </div>
                <div class="right">
                  <div class="odds-wrap bet-pick away-pick ${awayDisableStr}">
                    <div class="right odds ">${parseFloat(rateDetailInfo.arate).toFixed(2)}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          `;
        return $sel;
      }

      //승패
      function getRateInfo_12(detailInfo, i) {
        let rateDetailInfo = detailInfo.rateInfo[i];
        let betKey = getBetKeyStr(detailInfo.fixtureId, rateDetailInfo);
        let homeDisableStr = '';
        let awayDisableStr = '';
        let drawDisableStr = '';
        if (rateDetailInfo.rateHomeStatus != 1) {
          homeDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 0);
        }
        if (rateDetailInfo.rateAwayStatus != 1) {
          awayDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 0);
        }
        if (rateDetailInfo.rateDrawStatus != 1) {
          drawDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 0);
        }
        let pickTitle = detailInfo.homeTeamName + " vs " + detailInfo.awayTeamName;
        let pickDescHome = detailInfo.homeTeamName;
        let pickDescAway = detailInfo.awayTeamName;

        let $sel = `
          <div class="bet-info-${betKey}">
            <div class="option-wrap">
              <div class="option bet odds-${rateDetailInfo.hbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.hbetCode}', ${rateDetailInfo.hrate}, '', 0, '${pickDescHome}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
                <div class="left">
                  <div class="name">${detailInfo.homeTeamName}</div>
                </div>
                <div class="right">
                  <div class="odds-wrap bet-pick home-pick ${homeDisableStr}">
                    <div class="right odds ">${parseFloat(rateDetailInfo.hrate).toFixed(2)}</div>
                  </div>
                </div>
              </div>
              <div class="option bet odds-${rateDetailInfo.abetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.abetCode}', ${rateDetailInfo.arate}, '', 1, '${pickDescAway}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
                <div class="left">
                  <div class="name">${detailInfo.awayTeamName}</div>
                </div>
                <div class="right">
                  <div class="odds-wrap bet-pick away-pick ${awayDisableStr}">
                    <div class="right odds ">${parseFloat(rateDetailInfo.arate).toFixed(2)}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          `;
        return $sel;
      }

      //홀짝
      function getRateInfo_odd_even(detailInfo, i) {
        let rateDetailInfo = detailInfo.rateInfo[i];
        let betKey = getBetKeyStr(detailInfo.fixtureId, rateDetailInfo);
        let homeDisableStr = '';
        let awayDisableStr = '';
        let drawDisableStr = '';
        if (rateDetailInfo.rateHomeStatus != 1) {
          homeDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 0);
        }
        if (rateDetailInfo.rateAwayStatus != 1) {
          awayDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 0);
        }
        if (rateDetailInfo.rateDrawStatus != 1) {
          drawDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 0);
        }
        let pickTitle = detailInfo.homeTeamName + " vs " + detailInfo.awayTeamName;
        let pickDescHome = '홀';
        let pickDescAway = '짝';

        let $sel = `
          <div class="bet-info-${betKey}">
            <div class="option-wrap">
              <div class="option bet odds-${rateDetailInfo.hbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.hbetCode}', ${rateDetailInfo.hrate}, '', 0, '${pickDescHome}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
              <div class="left">
                <div class="name">홀 </div>
              </div>
              <div class="right">
                <div class="odds-wrap bet-pick home-pick ${homeDisableStr}">
                  <div class="right odds ">${parseFloat(rateDetailInfo.hrate).toFixed(2)}</div>
                </div>
              </div>
              </div>
              <div class="option bet odds-${rateDetailInfo.abetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.abetCode}', ${rateDetailInfo.arate}, '', 1, '${pickDescAway}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
              <div class="left">
                <div class="name">짝 </div>
              </div>
              <div class="right">
                <div class="odds-wrap bet-pick away-pick ${awayDisableStr}">
                  <div class="right odds ">${parseFloat(rateDetailInfo.arate).toFixed(2)}</div>
                </div>
              </div>
              </div>
            </div>
          </div>
          `;
        return $sel;
      }

      //언더오버
      function getRateInfo_under_over(detailInfo, i) {
        let rateDetailInfo = detailInfo.rateInfo[i];
        let betKey = getBetKeyStr(detailInfo.fixtureId, rateDetailInfo);
        let homeDisableStr = '';
        let awayDisableStr = '';
        let drawDisableStr = '';
        if (rateDetailInfo.rateHomeStatus != 1) {
          homeDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 0);
        }
        if (rateDetailInfo.rateAwayStatus != 1) {
          awayDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 0);
        }
        if (rateDetailInfo.rateDrawStatus != 1) {
          drawDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 0);
        }
        let pickTitle = detailInfo.homeTeamName + " vs " + detailInfo.awayTeamName;
        let pickDescHome = '언더 (' + rateDetailInfo.hline + ")";
        let pickDescAway = '오버 (' + rateDetailInfo.aline + ")";

        let $sel = `
          <div class="bet-info-${betKey}">
            <div class="option-wrap">
              <div class="option bet odds-${rateDetailInfo.hbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.hbetCode}', ${rateDetailInfo.hrate}, '', 0, '${pickDescHome}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
              <div class="left">
                <div class="name">언더&nbsp;[${rateDetailInfo.hline}]</div>
              </div>
              <div class="right">
                <div class="odds-wrap bet-pick home-pick ${homeDisableStr}">
                <div class="right odds ">${parseFloat(rateDetailInfo.hrate).toFixed(2)}</div>
                </div>
              </div>
              </div>
              <div class="option bet odds-${rateDetailInfo.abetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.abetCode}', ${rateDetailInfo.arate}, '', 1, '${pickDescAway}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
              <div class="left">
                <div class="name">오버&nbsp;[${rateDetailInfo.aline}]</div>
              </div>
              <div class="right">
                <div class="odds-wrap bet-pick away-pick ${awayDisableStr}">
                <div class="right odds ">${parseFloat(rateDetailInfo.arate).toFixed(2)}</div>
                </div>
              </div>
              </div>
            </div>
          </div>
          `;
        return $sel;
      }

      //아시안핸디캡, E-Sports 핸디캡
      function getRateInfo_handi(detailInfo, i) {
        let rateDetailInfo = detailInfo.rateInfo[i];
        let betKey = getBetKeyStr(detailInfo.fixtureId, rateDetailInfo);
        let homeDisableStr = '';
        let awayDisableStr = '';
        let drawDisableStr = '';
        if (rateDetailInfo.rateHomeStatus != 1) {
          homeDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 0);
        }
        if (rateDetailInfo.rateAwayStatus != 1) {
          awayDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 0);
        }
        if (rateDetailInfo.rateDrawStatus != 1) {
          drawDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 0);
        }
        let pickTitle = detailInfo.homeTeamName + " vs " + detailInfo.awayTeamName;
        let pickDescHome = detailInfo.homeTeamName + ' (' + rateDetailInfo.hline + ")";
        let pickDescAway = detailInfo.awayTeamName + ' (' + rateDetailInfo.aline + ")";
        let strLineScore = '';
        if (detailInfo.sportsCode == '6046') {
          strLineScore = rateDetailInfo.lineScore;
        }
        let $sel = `
          <div class="bet-info-${betKey}">
            <div class="option-wrap">
              <div class="option bet odds-${rateDetailInfo.hbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.hbetCode}', ${rateDetailInfo.hrate}, '', 0, '${pickDescHome}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
                <div class="left">
                  <div class="name">${detailInfo.homeTeamName}&nbsp;[${rateDetailInfo.hline}]&nbsp;${strLineScore}</div>
                </div>
                <div class="right">
                  <div class="odds-wrap bet-pick home-pick ${homeDisableStr}">
                  <div class="right odds ">${parseFloat(rateDetailInfo.hrate).toFixed(2)}</div>
                  </div>
                </div>
              </div>
              <div class="option bet odds-${rateDetailInfo.abetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.abetCode}', ${rateDetailInfo.arate}, '', 1, '${pickDescAway}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
                <div class="left">
                  <div class="name">${detailInfo.awayTeamName}&nbsp;[${rateDetailInfo.aline}]&nbsp;${strLineScore}</div>
                </div>
                <div class="right">
                  <div class="odds-wrap bet-pick away-pick ${awayDisableStr}">
                  <div class="right odds ">${parseFloat(rateDetailInfo.arate).toFixed(2)}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          `;
        return $sel;
      }

      //YesNo
      function getRateInfo_yes_no(detailInfo, i) {
        let rateDetailInfo = detailInfo.rateInfo[i];
        let betKey = getBetKeyStr(detailInfo.fixtureId, rateDetailInfo);
        let homeDisableStr = '';
        let awayDisableStr = '';
        let drawDisableStr = '';
        if (rateDetailInfo.rateHomeStatus != 1) {
          homeDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 0);
        }
        if (rateDetailInfo.rateAwayStatus != 1) {
          awayDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 0);
        }
        if (rateDetailInfo.rateDrawStatus != 1) {
          drawDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 0);
        }
        let pickTitle = detailInfo.homeTeamName + " vs " + detailInfo.awayTeamName;
        let pickDescHome = 'Yes';
        let pickDescAway = 'No';

        let $sel = `
          <div class="bet-info-${betKey}">
            <div class="option-wrap">
              <div class="option bet odds-${rateDetailInfo.hbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.hbetCode}', ${rateDetailInfo.hrate}, '', 0, '${pickDescHome}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
              <div class="left">
                <div class="name">Yes</div>
              </div>
              <div class="right">
                <div class="odds-wrap bet-pick home-pick ${homeDisableStr}">
                <div class="right odds ">${parseFloat(rateDetailInfo.hrate).toFixed(2)}</div>
                </div>
              </div>
              </div>
              <div class="option bet odds-${rateDetailInfo.abetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.abetCode}', ${rateDetailInfo.arate}, '', 1, '${pickDescAway}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
              <div class="left">
                <div class="name">No</div>
              </div>
              <div class="right">
                <div class="odds-wrap bet-pick away-pick ${awayDisableStr}">
                <div class="right odds ">${parseFloat(rateDetailInfo.arate).toFixed(2)}</div>
                </div>
              </div>
              </div>
            </div>
          </div>
          `;
        return $sel;
      }

      //정확한스코어
      function getRateInfo_correct_score(detailInfo, i) {
        let $sel = '';
        let rateDetailInfo = detailInfo.rateInfo[i];
        let betKey = getBetKeyStr(detailInfo.fixtureId, rateDetailInfo);
        let homeDisableStr = '';
        let awayDisableStr = '';
        let drawDisableStr = '';
        if (rateDetailInfo.rateHomeStatus != 1) {
          homeDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 0);
        }
        if (rateDetailInfo.rateAwayStatus != 1) {
          awayDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 0);
        }
        if (rateDetailInfo.rateDrawStatus != 1) {
          drawDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 0);
        }
        let pickTitle = detailInfo.homeTeamName + " vs " + detailInfo.awayTeamName;
        let pickDescHome = rateDetailInfo.hpickName;

        $sel = `
          <div class="option bet bet-info-${betKey} odds-${rateDetailInfo.hbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.hbetCode}', ${rateDetailInfo.hrate}, '', 0, '${pickDescHome}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
            <div class="left">
              <div class="name">${rateDetailInfo.hpickName} </div>
            </div>
            <div class="right odds-${rateDetailInfo.hbetCode}">
              <div class="odds-wrap bet-pick home-pick ${homeDisableStr}">
                <div class="right odds ">${parseFloat(rateDetailInfo.hrate).toFixed(2)}</div>
              </div>
            </div>
          </div>`;

        return $sel;
      }

      //double chance
      function getRateInfo_double_chance(detailInfo, i) {
        let rateDetailInfo = detailInfo.rateInfo[i];
        let betKey = getBetKeyStr(detailInfo.fixtureId, rateDetailInfo);
        let homeDisableStr = '';
        let awayDisableStr = '';
        let drawDisableStr = '';
        if (rateDetailInfo.rateHomeStatus != 1) {
          homeDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 0);
        }
        if (rateDetailInfo.rateAwayStatus != 1) {
          awayDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 0);
        }
        if (rateDetailInfo.rateDrawStatus != 1) {
          drawDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 0);
        }
        let pickTitle = detailInfo.homeTeamName + " vs " + detailInfo.awayTeamName;
        let pickDescHome = '홈 + 무';
        let pickDescDraw = '홈 + 원';
        let pickDescAway = '무 + 원';
        $sel = `
          <div class="option bet bet-info-${betKey} odds-${rateDetailInfo.hbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.hbetCode}', ${rateDetailInfo.hrate}, '', 0, '${pickDescHome}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
            <div class="left">
              <div class="name">${rateDetailInfo.hpickName} </div>
            </div>
            <div class="right odds-${rateDetailInfo.hbetCode}">
              <div class="odds-wrap bet-pick home-pick ${homeDisableStr}">
                <div class="right odds ">${parseFloat(rateDetailInfo.hrate).toFixed(2)}</div>
              </div>
            </div>
          </div>`;
        return $sel;
      }

      // 전반전/풀타임
      function getRateInfo_ht_ft(detailInfo, i) {
        let $sel = '';
        let rateDetailInfo = detailInfo.rateInfo[i];
        let betKey = getBetKeyStr(detailInfo.fixtureId, rateDetailInfo);
        let homeDisableStr = '';
        let awayDisableStr = '';
        let drawDisableStr = '';
        if (rateDetailInfo.rateHomeStatus != 1) {
          homeDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 0);
        }
        if (rateDetailInfo.rateAwayStatus != 1) {
          awayDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 0);
        }
        if (rateDetailInfo.rateDrawStatus != 1) {
          drawDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 0);
        }
        let pickTitle = detailInfo.homeTeamName + " vs " + detailInfo.awayTeamName;
        let pickDescHome = rateDetailInfo.hpickName;

        $sel = `
          <div class="option bet bet-info-${betKey} odds-${rateDetailInfo.hbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.hbetCode}', ${rateDetailInfo.hrate}, '', 0, '${pickDescHome}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
            <div class="left">
              <div class="name">${rateDetailInfo.hpickName} </div>
            </div>
            <div class="right odds-${rateDetailInfo.hbetCode}">
              <div class="odds-wrap bet-pick home-pick ${homeDisableStr}">
                <div class="right odds ">${parseFloat(rateDetailInfo.hrate).toFixed(2)}</div>
              </div>
            </div>
          </div>`;

        return $sel;
      }

      // 득점수
      function getRateInfo_number_of_goals(detailInfo, i) {
        let $sel = '';
        let rateDetailInfo = detailInfo.rateInfo[i];
        let betKey = getBetKeyStr(detailInfo.fixtureId, rateDetailInfo);
        let homeDisableStr = '';
        let awayDisableStr = '';
        let drawDisableStr = '';
        if (rateDetailInfo.rateHomeStatus != 1) {
          homeDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 0);
        }
        if (rateDetailInfo.rateAwayStatus != 1) {
          awayDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 0);
        }
        if (rateDetailInfo.rateDrawStatus != 1) {
          drawDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 0);
        }
        let pickTitle = detailInfo.homeTeamName + " vs " + detailInfo.awayTeamName;
        let pickDescHome = rateDetailInfo.hpickName;

        $sel = `
          <div class="option bet bet-info-${betKey} odds-${rateDetailInfo.hbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.hbetCode}', ${rateDetailInfo.hrate}, '', 0, '${pickDescHome}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
            <div class="left">
              <div class="name">${rateDetailInfo.hpickName} </div>
            </div>
            <div class="right odds-${rateDetailInfo.hbetCode}">
              <div class="odds-wrap bet-pick home-pick ${homeDisableStr}">
                <div class="right odds ">${parseFloat(rateDetailInfo.hrate).toFixed(2)}</div>
              </div>
            </div>
          </div>`;

        return $sel;
      }

      // 정확한 언더오버
      function getRateInfo_exactly_under_over(detailInfo, i) {
        let rateDetailInfo = detailInfo.rateInfo[i];
        let betKey = getBetKeyStr(detailInfo.fixtureId, rateDetailInfo);
        let homeDisableStr = '';
        let awayDisableStr = '';
        let drawDisableStr = '';
        if (rateDetailInfo.rateHomeStatus != 1) {
          homeDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 0);
        }
        if (rateDetailInfo.rateAwayStatus != 1) {
          awayDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 0);
        }
        if (rateDetailInfo.rateDrawStatus != 1) {
          drawDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 0);
        }
        let pickTitle = detailInfo.homeTeamName + " vs " + detailInfo.awayTeamName;
        let pickDescHome = '언더 (' + rateDetailInfo.hline + ")";
        let pickDescDraw = '정확한 (' + rateDetailInfo.hline + ")";
        let pickDescAway = '오버 (' + rateDetailInfo.aline + ")";

        let $sel = `
          <div class="bet-info-${betKey}">
            <div class="option-wrap">
              <div class="option bet odds-${rateDetailInfo.hbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.hbetCode}', ${rateDetailInfo.hrate}, '', 0, '${pickDescHome}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
              <div class="left">
                <div class="name">언더&nbsp;[${rateDetailInfo.hline}]</div>
              </div>
              <div class="right">
                <div class="odds-wrap bet-pick home-pick ${homeDisableStr}">
                <div class="right odds ">${parseFloat(rateDetailInfo.hrate).toFixed(2)}</div>
                </div>
              </div>
              </div>
              <div class="option bet odds-${rateDetailInfo.dbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.dbetCode}', ${rateDetailInfo.drate}, '', 2, '${pickDescDraw}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
                <div class="left">
                  <div class="name">정확한 </div>
                </div>
                <div class="right">
                  <div class="odds-wrap bet-pick draw-pick ${drawDisableStr}">
                    <div class="right odds ">${parseFloat(rateDetailInfo.drate).toFixed(2)}</div>
                  </div>
                </div>
              </div>
              <div class="option bet odds-${rateDetailInfo.abetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.abetCode}', ${rateDetailInfo.arate}, '', 1, '${pickDescAway}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
              <div class="left">
                <div class="name">오버&nbsp;[${rateDetailInfo.aline}]</div>
              </div>
              <div class="right">
                <div class="odds-wrap bet-pick away-pick ${awayDisableStr}">
                <div class="right odds ">${parseFloat(rateDetailInfo.arate).toFixed(2)}</div>
                </div>
              </div>
              </div>
            </div>
          </div>
          `;
        return $sel;
      }

      // 다음득점
      function getRateInfo_next_goal(detailInfo, i) {
        let rateDetailInfo = detailInfo.rateInfo[i];
        let betKey = getBetKeyStr(detailInfo.fixtureId, rateDetailInfo);
        let homeDisableStr = '';
        let awayDisableStr = '';
        let drawDisableStr = '';
        if (rateDetailInfo.rateHomeStatus != 1) {
          homeDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 0);
        }
        if (rateDetailInfo.rateAwayStatus != 1) {
          awayDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 0);
        }
        if (rateDetailInfo.rateDrawStatus != 1) {
          drawDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 0);
        }
        let pickTitle = detailInfo.homeTeamName + " vs " + detailInfo.awayTeamName;
        let goal = parseInt(rateDetailInfo.hline);
        let pickDescHome = goal + "골 홈팀";
        let pickDescDraw = goal + "골 없음";
        let pickDescAway = goal + "골 원정팀";

        let $sel = `
          <div class="bet-info-${betKey}">
            <div class="option-wrap">
              <div class="option bet odds-${rateDetailInfo.hbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.hbetCode}', ${rateDetailInfo.hrate}, '', 0, '${pickDescHome}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
              <div class="left">
                <div class="name">홈팀 ${goal}골&nbsp;</div>
              </div>
              <div class="right">
                <div class="odds-wrap bet-pick home-pick ${homeDisableStr}">
                <div class="right odds ">${parseFloat(rateDetailInfo.hrate).toFixed(2)}</div>
                </div>
              </div>
              </div>
              <div class="option bet odds-${rateDetailInfo.dbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.dbetCode}', ${rateDetailInfo.drate}, '', 2, '${pickDescDraw}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
                <div class="left">
                  <div class="name">${goal}골 없음 </div>
                </div>
                <div class="right">
                  <div class="odds-wrap bet-pick draw-pick ${drawDisableStr}">
                    <div class="right odds ">${parseFloat(rateDetailInfo.drate).toFixed(2)}</div>
                  </div>
                </div>
              </div>
              <div class="option bet odds-${rateDetailInfo.abetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.abetCode}', ${rateDetailInfo.arate}, '', 1, '${pickDescAway}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
              <div class="left">
                <div class="name">원정팀 ${goal} 골&nbsp;</div>
              </div>
              <div class="right">
                <div class="odds-wrap bet-pick away-pick ${awayDisableStr}">
                <div class="right odds ">${parseFloat(rateDetailInfo.arate).toFixed(2)}</div>
                </div>
              </div>
              </div>
            </div>
          </div>
          `;
        return $sel;
      }

      //승무패 및 언더오버
      function getRateInfo_1x2_under_over(detailInfo, i) {
        let rateDetailInfo = detailInfo.rateInfo[i];
        let betKey = getBetKeyStr(detailInfo.fixtureId, rateDetailInfo);
        let homeDisableStr = '';
        let awayDisableStr = '';
        let drawDisableStr = '';
        if (rateDetailInfo.rateHomeStatus != 1) {
          homeDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 0);
        }
        if (rateDetailInfo.rateAwayStatus != 1) {
          awayDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 0);
        }
        if (rateDetailInfo.rateDrawStatus != 1) {
          drawDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 0);
        }

        let pickKorName = '';
        switch (rateDetailInfo.hpickName) {
          case "1 And Under":
            pickKorName = "홈승 & 언더";
            break;
          case "1 And Over":
            pickKorName = "홈승 & 오버";
            break;
          case "2 And Under":
            pickKorName = "원정승 & 언더";
            break;
          case "2 And Over":
            pickKorName = "원정승 & 오버";
            break;
          case "X And Under":
            pickKorName = "무 & 언더";
            break;
          case "X And Over":
            pickKorName = "무 & 오버";
            break;
        }
        let pickTitle = detailInfo.homeTeamName + " vs " + detailInfo.awayTeamName;
        let pickDescHome = pickKorName + " (" + rateDetailInfo.hline + ")";

        let $sel = `
            <div class="option bet bet-info-${betKey} odds-${rateDetailInfo.hbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.hbetCode}', ${rateDetailInfo.hrate}, '', 0, '${pickDescHome}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
              <div class="left">
                <div class="name">${pickKorName}&nbsp;${rateDetailInfo.hline} </div>
              </div>
              <div class="right odds-${rateDetailInfo.hbetCode}">
                <div class="odds-wrap bet-pick home-pick ${homeDisableStr}">
                <div class="right odds ">${parseFloat(rateDetailInfo.hrate).toFixed(2)}</div>
                </div>
              </div>
            </div>
          `;
        return $sel;
      }

      // 유럽핸디캡
      function getRateInfo_european_handicap(detailInfo, i) {
        let rateDetailInfo = detailInfo.rateInfo[i];
        let betKey = getBetKeyStr(detailInfo.fixtureId, rateDetailInfo);
        let homeDisableStr = '';
        let awayDisableStr = '';
        let drawDisableStr = '';
        if (rateDetailInfo.rateHomeStatus != 1) {
          homeDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.hbetCode, 0);
        }
        if (rateDetailInfo.rateAwayStatus != 1) {
          awayDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.abetCode, 0);
        }
        if (rateDetailInfo.rateDrawStatus != 1) {
          drawDisableStr = "icon-iconLock";
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 2);
        } else {
          setBetPickStatusBySpBetCode(rateDetailInfo.dbetCode, 0);
        }
        let pickTitle = detailInfo.homeTeamName + " vs " + detailInfo.awayTeamName;
        let pickDescHome = '홈 (' + rateDetailInfo.hline + ")";
        let pickDescDraw = '무 (' + rateDetailInfo.hline + ")";
        let pickDescAway = '원 (' + rateDetailInfo.aline + ")";

        let $sel = `
          <div class="bet-info-${betKey}">
            <div class="option-wrap">
              <div class="option bet odds-${rateDetailInfo.hbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.hbetCode}', ${rateDetailInfo.hrate}, '', 0, '${pickDescHome}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
              <div class="left">
                <div class="name">${detailInfo.homeTeamName}&nbsp;[${rateDetailInfo.bline}]</div>
              </div>
              <div class="right">
                <div class="odds-wrap bet-pick home-pick ${homeDisableStr}">
                <div class="right odds ">${parseFloat(rateDetailInfo.hrate).toFixed(2)}</div>
                </div>
              </div>
              </div>
              <div class="option bet odds-${rateDetailInfo.dbetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.dbetCode}', ${rateDetailInfo.drate}, '', 2, '${pickDescDraw}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
                <div class="left">
                  <div class="name">무승부</div>
                </div>
                <div class="right">
                  <div class="odds-wrap bet-pick draw-pick ${drawDisableStr}">
                    <div class="right odds ">${parseFloat(rateDetailInfo.drate).toFixed(2)}</div>
                  </div>
                </div>
              </div>
              <div class="option bet odds-${rateDetailInfo.abetCode}" data-role="bet" onclick="selectPick('${betKey}', ${detailInfo.fixtureId}, '${detailInfo.sportsCode}', '${detailInfo.sportsName}', '${rateDetailInfo.abetCode}', ${rateDetailInfo.arate}, '', 1, '${pickDescAway}', '${rateDetailInfo.marketName}', '${pickTitle}', ${rateDetailInfo.marketId})">
              <div class="left">
                <div class="name">${detailInfo.awayTeamName}&nbsp;[${rateDetailInfo.bline}]</div>
              </div>
              <div class="right">
                <div class="odds-wrap bet-pick away-pick ${awayDisableStr}">
                <div class="right odds ">${parseFloat(rateDetailInfo.arate).toFixed(2)}</div>
                </div>
              </div>
              </div>
            </div>
          </div>
          `;
        return $sel;
      }

      function filterMarket(marketName) {
        $('.detail-wrap .reverse .type-wrap .type').attr("selected", false);
        $('.detail-wrap .reverse .type-wrap [data-type="' + marketName + '"]').attr("selected", true);

        if (marketName == '') {
          $(".detail-wrap .reverse .option-group-wrap .option-group").removeClass("hide");
        }
        if (marketName == '언더오버') {
          $(".detail-wrap .reverse .option-group-wrap .option-group").addClass("hide");
          $(".detail-wrap .reverse .option-group-wrap .market-family-7").removeClass("hide");
        }
        if (marketName == '핸디캡') {
          $(".detail-wrap .reverse .option-group-wrap .option-group").addClass("hide");
          $(".detail-wrap .reverse .option-group-wrap .market-family-8").removeClass("hide");
          $(".detail-wrap .reverse .option-group-wrap .market-family-9").removeClass("hide");
          $(".detail-wrap .reverse .option-group-wrap .market-family-14").removeClass("hide");
        }
        if (marketName == '기타') {
          $(".detail-wrap .reverse .option-group-wrap .option-group").removeClass("hide");
          $(".detail-wrap .reverse .option-group-wrap .market-family-7").addClass("hide");
          $(".detail-wrap .reverse .option-group-wrap .market-family-8").addClass("hide");
          $(".detail-wrap .reverse .option-group-wrap .market-family-9").addClass("hide");
          $(".detail-wrap .reverse .option-group-wrap .market-family-14").addClass("hide");
        }
      }

      function getBettingList() {
        ajaxSend('/front/game/bet_list/list.asp', {
          type: game_type,
          start: pg_start
        }, function(jData) {
          if (!jData.success) {
            showMsg(jData.message || "정보 오류!", 'error');
            return;
          }

          let data = jData.data;
          let $sel = '';

          if (data.list && data.list.length > 0 && data.detailList && data.detailList.length > 0) {
            for (let i = 0; i < data.list.length; i++) {
              let folderCnt = 0;
              let betInfo = data.list[i];
              let shotBetDate = String(betInfo.betDate).substr(5);
              $sel += `<div class="folder many">`
              for (let j = 0; j < data.detailList.length; j++) {
                let betDetailInfo = data.detailList[j];
                if (betInfo.id == betDetailInfo.betId) {
                  folderCnt++;
                  let shortStartDate = String(betDetailInfo.startDate).substr(5);

                  let resultStr = '';
                  let resultClass = '';
                  if (betDetailInfo.status == 0) {
                    resultStr = "대기";
                    resultClass = "bet-status-wait";
                  } else if (betDetailInfo.status == 1) {
                    resultStr = "낙첨";
                    resultClass = "bet-status-lose";
                  } else if (betDetailInfo.status == 2) {
                    resultStr = "당첨";
                    resultClass = "bet-status-win";
                  } else if (betDetailInfo.status == 4) {
                    resultStr = "무효";
                    resultClass = "bet-status-off";
                  } else if (betDetailInfo.status == 3 || betDetailInfo.status == 5) {
                    resultStr = "취소";
                    resultClass = "bet-status-cancel";
                  }
                  let selPickName = betDetailInfo.selPickName;
                  $sel += `
                    <div class="title">
                      <div class="datetime">${shotBetDate}</div>
                      <div class="status">${betDetailInfo.gameType}</div>
                    </div>
                    <div class="content-wrap">
                    <div class="status ${resultClass}">${resultStr}</div>
                    <div class="content">
                        <div class="name-wrap">
                          <div class="name">${betDetailInfo.homeTeamName} vs ${betDetailInfo.awayTeamName}</div>
                          <div class="button-wrap"> 
                          
                          </div>
                        </div>
                        <div class="datetime"> ${shortStartDate} </div>
                        <div class="bet">
                        <div class="market"> ${betDetailInfo.marketName} </div>
                        <div class="pick">${selPickName} ${betDetailInfo.baseLine}</div>
                        <div class="odds">@ ${betDetailInfo.selectRate}</div>
                        </div>
                    </div>
                    </div>
                    `;
                }
              }

              let resultStr = '';
              let resultClass = '';
              let showCancelBtn = false;
              if (betInfo.status == 0) {
                resultStr = "대기";
                resultClass = "bet-status-wait";
                showCancelBtn = true;
              } else if (betInfo.status == 1) {
                resultStr = "낙첨";
                resultClass = "bet-status-lose";
              } else if (betInfo.status == 2) {
                resultStr = "당첨";
                resultClass = "bet-status-win";
              } else if (betInfo.status == 4) {
                resultStr = "무효";
                resultClass = "bet-status-off";
              } else if (betInfo.status == 3 || betInfo.status == 5) {
                resultStr = "취소";
                resultClass = "bet-status-cancel";
              }
              let expWincash = formatComma(parseFloat(betInfo.cashBet * (betInfo.rateBet + betInfo.rateBonus)).toFixed());
              let cashBetComma = formatComma(betInfo.cashBet);

              let selRate = '';
              selRate = betInfo.rateBet;
              if (betInfo.rateBonus != 0) {
                selRate += "+" + betInfo.rateBonus;
              }
              $sel += `
                <div class="result">
                  <span>폴더수 ${folderCnt}</span>
                  <span>총 배당률 ${selRate}</span>
                  <div class="button-wrap"> 
                    <!--button class="cancel-btn ${showCancelBtn ? "" : "hide"}" onclick="cancelSportsGameBet(${betInfo.id})"> 취소 </button-->
                    <button class="cancel-btn ${showCancelBtn ? "hide" : ""}" onclick="delSportsGameBet(${betInfo.id})"> 삭제 </button>
                  </div>
                </div>
                <div class="result2">
                  <span>베팅금 <div><span class="sp-number"> ${cashBetComma} </span> 원</div></span>
                  <span>예상 당첨금 <div><span class="sp-number"> ${expWincash} </span> 원</div></span>
                  <div class="status ${resultClass}">${resultStr}</div>
                </div>
              </div>`;
            }
            if (pg_start == 0) {
              $(".betting-list-option .folder-wrap").html($sel);
            } else {
              $(".betting-list-option .folder-wrap").append($sel);
            }
          } else {
            if (pg_start == 0) $(".betting-list-option .folder-wrap").empty();
          }
          pg_start += data.length;
          if (pg_start < data.total) {
            $(".betting-list-option .history .scroll .more").removeClass("disabled");
          } else {
            $(".betting-list-option .history .scroll .more").addClass("disabled");
          }
        });
      }

      function cancelSportsGameBet(betId) {
        confirmMsgYn("취소하시겠습니까?", function() {
          ajaxSend('/front/game/sports/cancel_bet.asp', {
            id: betId
          }, function(jData) {
            if (!jData.success) {
              showMsg(jData.message || "취소 실패하였습니다.", 'error');
              return;
            }
            showMsg("취소되었습니다,", 'success');

            let cashAmount = formatComma(jData.data);
            $('.userCashAmount').each(function() {
              if ($(this).prop("tagName") == 'INPUT') {
                $(this).val(cashAmount);
                return;
              }
              $(this).html(cashAmount);
            });
            pg_start = 0;
            getBettingList();
          });
        })
      }

      function initStartPage() {
        pg_start = 0;
      }

      function delSportsGameBet(betId) {
        confirmMsgYn("삭제하시겠습니까?", function() {
          ajaxSend('/front/game/sports/delete.asp', {
            id: betId
          }, function(jData) {
            if (!jData.success) {
              showMsg(jData.message || "삭제 실패하였습니다.", 'error');
              return;
            }
            showMsg("삭제되었습니다,", 'success');
            pg_start = 0;
            getBettingList();
          });
        })
      }
    </script>
  </div>

</body>

</html>