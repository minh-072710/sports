<!--#include virtual="/include/dbcon.asp"-->
<%
response.ContentType = "application/json"
idx = ReForm("id")

sql = "select * from tb_total_cart a left join tb_member b on a.mem_idx = b.idx where a.isCanceled = 'N' and a.idx = '" & idx & "'"
set rs = dbcon.execute(sql)
if rs.eof then
  Response.write "{""success"":false,""message"":""잘못된 접근입니다.""}"
  Response.end

'time_left - 25 - close_time
else
  '베팅정보
  mem_id = rs("mem_id")
  mem_idx = rs("mem_idx")
  betting_money = rs("betting_money")
  sports_type = rs("sports_type")
  game_no = rs("game_no")
  '유저정보
  g_money = rs("g_money")
  point = rs("point")
  userLev = rs("lev")
  total_money = CLng(g_money) + CLng(betting_money)
end if
rs.close

if mem_id<>Session("IU_ID") then 
  Response.write "{""success"":false,""message"":""로그인를 해주세요.""}"
  Response.end
end if

sql = "select top 1 * from view_betting_parent with(nolock) where game_no = '"&game_no&"'"
set rs = dbcon.execute(sql)
if rs.eof then
  Response.write "{""success"":false,""message"":""잘못된 접근입니다.""}"
  Response.end
else
  game_time = rs("game_time")
  left_time = DateDiff("s", now(), game_time)

  if left_time<0 then
    Response.write "{""success"":false,""message"":""이미 마감되었습니다.""}"
    Response.end
  end if
end if

Dbcon.BeginTrans

' 돈 돌려줌
Insql = "UPDATE tb_member set g_money = g_money + "&betting_money&" where idx = '"&mem_idx&"'"
dbcon.execute Insql

' 포인트 내역
sql = "INSERT INTO tb_point (memidx, kubun, code, amount_money, amount_point, g_jan, p_jan, content, reason, regdate, admin ) values('"&mem_idx&"', '보유금액증가', '1', '"&betting_money&"', '0', '"&total_money&"', '"&point&"', '베팅취소', '"&game_no&"', getDate(), '"&SESSION("IU_ID")&"')"
dbcon.execute(sql)

' 내역 변경
Insql = "UPDATE tb_total_cart set isCanceled = 'Y' where game_no = '"&game_no&"'"
dbcon.execute Insql

' Parent 걸린 금액 차감
If userLev = "1" Or userLev = "2" Then
  sql = "select * from tb_total_betting where game_no = '"&game_no&"'"
  rs2.open sql,dbcon,1,1
  If Not rs2.eof Then
    Do While Not rs2.eof
      choice = rs2("GameSelect")
      parent_idx = rs2("parent_idx")
      sql = "update tb_parent set money"&choice&" = money"&choice&" - "&betting_money& " where idx = '"&parent_idx&"'"
      dbcon.execute sql
      rs2.movenext
    Loop
  End If
  rs2.close
End if

If dbcon.Errors.Count=0 Then
  dbcon.CommitTrans      
  dbcon.close
  Response.write "{""success"":true, ""data"":"& total_money &"}"
Else
  dbcon.RollBackTrans
  dbcon.close
End If
Response.end
%>