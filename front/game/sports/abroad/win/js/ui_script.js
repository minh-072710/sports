$.modal.defaults = {
  clickClose: false,
};

$(document).ready(function () {

  $(document).on("mousedown", "div.blocker.current", function (e) {
    if ($(e.target).hasClass("blocker")) $.modal.close();
  });

  $(".icon-x-modal").click(function () {
    $.modal.close();
  });

  $("body").on('click', '.login-link', function () {
    console.log("open login")
    closeAllPopups();
    openBgPopup($("#login_pop"));
  });
  $("body").on('click', '.join-link', function () {
    closeAllPopups();
    openBgPopup($("#join_pop"));
  });
  ////////////////////////////////

  $("#app, header").on("click", ".play_live", function (e) {
    e.preventDefault();
    e.stopPropagation();

    if (!chkSignedIn()) {
      return;
    }
    let title = $(this).data('title');
    let code = $(this).data('code');

    openPopupGameText("Loading...");
    ajaxSend('/front/game/play/live.json', {
      vendor_code: code
    }, function (data) {
      if (data.success) {
        openPopupGameUrl("/front/game/play?title=" + title + "&key=" + data.message + "&vendor=" + code);
        // openPopupGameUrl(data.message);
      } else {
        openPopupGameText(data.message);
      }
    }, undefined, function (data) {
      if (!data.success && data.retCode == ERROR_UNREAD_LETTER) {
        closePopupGame();
      }
    })
  });

  $("#app, header").on("click", ".lvl-link", function () {
    e.preventDefault();
    e.stopPropagation();

    if (!chkSignedIn()) {
      return;
    }
    let $wrapper = $(this);
    let title = $wrapper.data('title');
    // openPopupGameText("Loading...");
    loading.open();
    ajaxSend('/front/game/live_lvl', {
      vendor_code: $wrapper.data('code')
    }, function (data) {
      if (data.success === undefined) {
        $("#casino_lvl_pop .popup-box>.box").html(data);
        closeAllPopups();
        openBgPopup($("#casino_lvl_pop"));
      } else if (!data.success) {
        if (data.message) confirmMsgOnly(data.message);
      }
    }, undefined, function () {
      loading.close();
    });
  });

  $('header').on('click', '.menu', function () {
    if ($('.slide-content-left').hasClass('active')) {
      $('.slide-content-left').removeClass('active');
      $('#app, header').removeClass('slide-left');
    } else {
      $('.slide-content-left').addClass('active');
      $('#app, header').addClass('slide-left');
    }
  })

  ////////////////////////////////

  $('body').on('keydown', '.content-editable', function (e) {
    if (e.keyCode == 9) {
      // document.execCommand('insertHTML', false, '\t');
      // e.preventDefault();
    } else if (e.keyCode == 13) {
      document.execCommand('formatBlock', false, 'p');
    }
  });

  rebuildTinymce();

  $(window).scroll(function () {
    // let height = $(document).height() - $(window).height();
    // let limitTop = height / 2;
    let limitTop = 50;
    if ($(document).scrollTop() > limitTop) {
      $(".quick .top").fadeIn("fast");
    } else {
      $(".quick .top").fadeOut("fast");
    }
    var topset = $(window).scrollTop();
    if ($("#betslipmove").children("span").hasClass("on active")) {
      console.log(11);
      if (topset < 260) {
        $(".right-box").css("top", 0);
      } else {
        $(".right-box").css("top", $(window).scrollTop() - 260);
      }
    }
  });
});

function rebuildTinymce(extraOpt = {}) {
  let options = {
    mode: "specific_textareas",
    editor_selector: "reqcontent",
    language: "ko_KR",
    plugins: [
      "advlist", "autolink", "lists", "link", "image", "charmap", "print", "preview", "anchor", "searchreplace", "visualblocks", "code", "fullscreen", "insertdatetime", "media", "table", "paste", "code", "help", "wordcount", "save", "autoresize"
    ],
    toolbar:
      "fontselect | fontsizeselect | forecolor | bold italic underline | alignjustify alignleft aligncenter alignright |  numlist | table tabledelete | link image",
    toolbar_mode: "wrap",

    menubar: false,
    // statusbar: false,
    branding: false,
    elementpath: false,
    relative_urls: false,
    remove_script_host: false,
    convert_urls: true,
    image_advtab: false,
    paste_data_images: true,
    mobile: {
      plugins: [
        "advlist", "autolink", "lists", "link", "image", "charmap", "print", "preview", "anchor", "searchreplace", "visualblocks", "code", "fullscreen", "insertdatetime", "media", "table", "paste", "code", "help", "wordcount", "save", "autoresize"
      ],
      toolbar:
        "fontselect | fontsizeselect | forecolor | bold italic underline | alignjustify alignleft aligncenter alignright |  numlist | table tabledelete | link image",
      toolbar_mode: "wrap",

      menubar: false,
      // statusbar: false,
      branding: false,
      elementpath: false,
      relative_urls: false,
      remove_script_host: false,
      convert_urls: true,

      image_advtab: false,
      placeholder: "내용을 입력하세요.",
    },
    content_style: `
					.mce-content-body[data-mce-placeholder]:not(.mce-visualblocks)::before {
						color: rgb(255 255 255 / 30%);
					}`,
  };
  for (let key in extraOpt) {
    let val = extraOpt[key];
    if (key == "dark_mode") {
      options.skin = "sublime-dark";
      options.content_css = "sub-dark";
    } else if (key == "image_upload") {
      options.images_upload_handler = function (blobInfo, success, failure) {
        let xhr, formData;

        xhr = new XMLHttpRequest();
        xhr.withCredentials = true;
        xhr.open('POST', "/common/upload/image");

        xhr.onload = function () {
          if (xhr.status != 200) {
            failure('HTTP Error: ' + xhr.status);
            return;
          }
          let jData = JSON.parse(xhr.responseText);
          if (!jData) {
            failure("파일업로드에서 오류가 발생하였습니다.");
            return;
          }
          if (!jData.success) {
            failure(jData.message);
            return;
          }

          success(/* settings.url_image_base + */jData.data);

        };
        console.log(xhr);

        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
        // form_data.append("name", this._elemAtachName.val());

        xhr.send(formData);
      };
    } else if (key == "init_callback") {
      options.init_instance_callback = val;
    } else if (key == "toolbar") {
      options.toolbar = val;
      options.mobile.toolbar = val;
    } else {
      options[key] = val;
    }
  }

  // tinymce
  if ($("textarea.reqcontent").length > 0) {
    tinymce.remove();
    tinymce.init(options);
  }
}

function validateParsley($form) {
  $form.parsley().validate();
  return $form.parsley().isValid();
}

function closePopup(sel) {
  closeAllPopups(sel);
}

function closeAllPopups(sel) {
  sel = sel || "";
  if (sel) {
    let $sel = sel;
    if (!($sel instanceof jQuery)) {
      $sel = $(sel);
    }
    while (1) {
      let ret = $.modal.close();
      if ($sel.is(ret)) break;
    }
    return;
  }

  while ($.modal.close());
}

function openBgPopup($sel) {
  $sel.modal({
    fadeDuration: 300,
    showClose: false,
    closeExisting: false,
  });
  $sel.find(":input:not([type=hidden])").eq(0).focus();
}

function refreshHref(href, force = true) {
  let reload = false;
  if (force) {
    if (href == location.hash) {
      reload = true;
    }
  }
  location.href = href;
  if (!reload) return;

  if (typeof onHashChange == 'function') {
    onHashChange();
    return;
  }
  location.reload();
}