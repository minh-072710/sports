var totalRate = 0;
var betConfirmMsg = false;
var betConfirm = true;
var mapAllowMarketLimitIds = new Map();
var mapStrAllowMarketLimitIds = new Map();
var mapNotAllowMarketLimitIds = new Map();
var mapMarketLimitUseYn = new Map();

function initBettingCartInputComma() {              //확인
  // inputbox comma formmating
  $.fn.commaTextbox = function () {
    var applyFormatting = function (that) {
      var caretPosition = that.selectionStart
      var origVal = $(that).val();
      $('#origVal').text(origVal); // Temporary
      var justNumbers = origVal.replace(/[^\-1234567890\.]/g, "");
      $(that).attr('data-raw-value', justNumbers);
      $('#justNumbers').text(justNumbers); // Temporary
      if (justNumbers.length == 0) {
        $(that).val('');

        betCash = 0;
        hitCash = 0;
        if (getQuickBetStatus() == 0) {
          $('#betting_cart .monitor .state-wrap').find("input[name=input_bet_cash]").val("");
          $('#betting_cart .monitor .state-wrap .bet-money .value .cash-number').html(0);
        }
        else {
          $(".quickbetting .top .money input[name=money]").val("");
        }
        return;
      }

      // Get rid of the decimal place and capture separately
      var decimalRegex = /(-?\d*)(\.(\d*)?)?/g
      var decimalPartMatches = decimalRegex.exec(justNumbers);
      var decimalPart = "";
      if (decimalPartMatches[2]) {
        decimalPart = decimalPartMatches[2];
      }
      $('#decimalPart').text(decimalPart); // Temporary
      var withoutDecimal = decimalPartMatches[1];
      $('#withoutDecimal').text(withoutDecimal); // Temporary

      // Assemble the final formatted value and put it in
      var final = '';
      final += withoutDecimal.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      final += decimalPart;
      $(that).val(final);
      $('#final').text(final); // Temporary

      // Figure out new caret position and restore it
      var origSelOffset = origVal.length - justNumbers.length;
      var selPosInNumber = caretPosition - origSelOffset;
      var newSelOffset = final.length - justNumbers.length;
      var newSelPos = selPosInNumber + newSelOffset;
      that.setSelectionRange(newSelPos, newSelPos);

      betCash = parseInt(justNumbers);
      hitCash = parseFloat(betCash * parseFloat(totalRate)).toFixed(0);
      if (getQuickBetStatus() == 0) {
        $('#betting_cart .monitor .state-wrap').find("input[name=input_bet_cash]").val(formatComma(betCash));
        $('#betting_cart .monitor .state-wrap .bet-money .value .cash-number').html(formatComma(hitCash));
      }
      else {
        $(".quickbetting .top .money input[name=money]").val(formatComma(betCash));
        quickBetCash = betCash;
      }
    };

    this.each(function () {
      applyFormatting(this);
    });

    $(this).on('input.commaTextbox', function (event) {
      applyFormatting(this);
    });

    return this;
  };
  $('input[data-type="comma"]').commaTextbox();
}


function bettingMoneyPlus(bonusCash) {                // 확인
  let $input = $('#betting_cart .monitor .state-wrap').find("input[name=input_bet_cash]");
  let betCash = parseCommaInteger($input.val() || 0) + parseInt(bonusCash);
  $input.val(formatComma(betCash));
  let hitCash = parseFloat(betCash * parseFloat(totalRate)).toFixed(0);
  $('#betting_cart .monitor .state-wrap .bet-money .value .cash-number').html(formatComma(hitCash));
}

function clearMoney() {                       //확인
  $('#betting_cart .monitor .state-wrap').find("input[name=input_bet_cash]").val(0);
  $('#betting_cart .monitor .state-wrap .bet-money .value .cash-number').html(0);
}

function bettingAllIn() {
  let maxCash = 0;
  let pick_len = m_sportsBetList.pick_list.length || 0;
  if (totalRate > 0) {
    if (pick_len > 1) {
      let maxWin = parseInt(multiMaxWinCash / parseFloat(totalRate));
      maxCash = Math.min(myCash, multiMaxBetCash, maxWin);
    } else {
      let maxWin = parseInt(singleMaxWinCash / parseFloat(totalRate));
      maxCash = Math.min(myCash, singleMaxBetCash, maxWin);
    }
  }
  $('#betting_cart .monitor .state-wrap').find("input[name=input_bet_cash]").val(formatComma(maxCash));
  let hitCash = parseFloat(maxCash * parseFloat(totalRate)).toFixed(0);
  $('#betting_cart .monitor .state-wrap .bet-money .value .cash-number').html(formatComma(hitCash));
}

function initRestriction() {
  ajaxSend('/front/game/user/info.asp'
    , {}
    , function (jData) {
      if (!jData.success) {
        //showMsg(jData.message || "전보갱신 실패하였습니다.", 'error');
        return;
      }
      if (jData.data) {
        myCash = jData.data.mycash;
        $('.userCashAmount').each(function () {
          if ($(this).prop("tagName") == 'INPUT') {
            $(this).val(formatComma(myCash));
            return;
          }
          $(this).html(formatComma(myCash));
        });
        let cashPoint = formatComma(jData.data.mypoint);
        $('.userCashPoint').each(function () {
          if ($(this).prop("tagName") == 'INPUT') {
            $(this).val(cashPoint);
            return;
          }
          $(this).html(cashPoint);
        });
        if (jData.data.singleMinBetCash > 0) {
          minBetCash = jData.data.singleMinBetCash;
          $("#betting_cart .restriction-wrap .single-min-cash-bet .number").html(formatComma(minBetCash));
        }
        if (jData.data.singleMaxBetCash > 0) {
          singleMaxBetCash = jData.data.singleMaxBetCash;
          $("#betting_cart .restriction-wrap .single-max-cash-bet .number").html(formatComma(singleMaxBetCash));
        }
        if (jData.data.singleMaxWinCash > 0) {
          singleMaxWinCash = jData.data.singleMaxWinCash;
          $("#betting_cart .restriction-wrap .single-max-cash-win .number").html(formatComma(singleMaxWinCash));
        }
        if (jData.data.multiMinBetCash > 0) {
          multiMinBetCash = jData.data.multiMinBetCash;
          $("#betting_cart .restriction-wrap .multi-min-cash-bet .number").html(formatComma(multiMinBetCash));
        }
        if (jData.data.multiMaxWinCash > 0) {
          multiMaxWinCash = jData.data.multiMaxWinCash;
          $("#betting_cart .restriction-wrap .multi-max-cash-win .number").html(formatComma(multiMaxWinCash));
        }
        if (jData.data.multiMaxBetCash > 0) {
          multiMaxBetCash = jData.data.multiMaxBetCash;
          $("#betting_cart .restriction-wrap .multi-max-cash-bet .number").html(formatComma(multiMaxBetCash));
        }
        let betCash = 0;
        betCash = parseCommaInteger($('#betting_cart .monitor .state-wrap').find("input[name=input_bet_cash]").val() || 0);
        if (minBetCash > 0 && betCash == 0) {
          bettingMoneyPlus(minBetCash);
        }
      }
    });
}

function initBetValue() {     // 확인
  totalRate = 0;
  $('#betting_cart .monitor .state-wrap .bet-odds .value').html("0.00");
  $('#betting_cart .monitor .state-wrap .bet-money .value .cash-number').html("0");
  $('#betting_cart .monitor .state-wrap').find("input[name=input_bet_cash]").val(0);
  initRestriction();
}

function getPickPosInBetCart(betCode) {
  let pos = -1;
  for (let i = 0; i < m_sportsBetList.pick_list.length; i++) {
    if (m_sportsBetList.pick_list[i].bet_code == betCode) {
      pos = i;
      break;
    }
  }
  return pos;
}

function removePickFromBetCart(betCode) {
  let pos = getPickPosInBetCart(betCode);
  if (pos >= 0) {
    m_sportsBetList.pick_list.splice(pos, 1);
  }

  if ($(".odds-" + betCode).hasClass('pick')) {
    $(".odds-" + betCode).removeClass('pick');
  }
}

function setBetValue(pTotalRate) {
  let $input = $('#betting_cart .monitor .state-wrap').find("input[name=input_bet_cash]");
  pTotalRate = Math.floor(pTotalRate * 100) / 100;
  totalRate = pTotalRate;
  $('#betting_cart .monitor .state-wrap .bet-odds .value').html(pTotalRate);
  let betCash = parseCommaInteger($input.val() || 0)
  let hitCash = parseFloat(betCash * pTotalRate).toFixed(0);
  $('#betting_cart .monitor .state-wrap .bet-money .value .cash-number').html(formatComma(hitCash));
}

function rebuildBetCart() {
  let totalRate = 1.0;
  $("#betting_cart .betting-pick-option .folder-wrap").empty();

  for (let i = 0; i < m_sportsBetList.pick_list.length; i++) {
    if (m_sportsBetList.pick_list[i].status < 2) {
      appendPickToBetCart(m_sportsBetList.pick_list[i]);
      totalRate *= parseFloat(m_sportsBetList.pick_list[i].select_rate);
    } else {
      //removePickFromBetCart(m_sportsBetList.pick_list[i].bet_code); //종료된 게임 삭제
      //rebuildBetCart(); //종료된 게임 삭제후 카트 갱신
    }
  }
  $("#betting_cart_opener b").html(m_sportsBetList.pick_list.length);
  $("#betting_cart .bet-cart-cnt").html(m_sportsBetList.pick_list.length);
  setBetValue(totalRate);
  //getGameList();  //종료된 게임 삭제후 리스트 갱신
}

function deleteSameLevelPick(fixtureId, marketId) {
  if (m_sportsBetList.pick_list.length > 0) {
    for (let i = 0; i < m_sportsBetList.pick_list.length; i++) {
      if (fixtureId == m_sportsBetList.pick_list[i].fixture_id && marketId == m_sportsBetList.pick_list[i].marketId) {
        removePickFromBetCart(m_sportsBetList.pick_list[i].bet_code);
      }
    }
  }
  rebuildBetCart();
}

function setBetPickStatusBySpBetCode(betCode, status) {
  if (m_sportsBetList.pick_list.length > 0) {
    for (let i = 0; i < m_sportsBetList.pick_list.length; i++) {
      if (betCode == m_sportsBetList.pick_list[i].bet_code) {
        m_sportsBetList.pick_list[i].status = status;
      }
    }
  }
}

function showBetPickStatus() {
  if (m_sportsBetList.pick_list.length > 0) {
    for (let i = 0; i < m_sportsBetList.pick_list.length; i++) {
      if (m_sportsBetList.pick_list[i].status > 0) {
        $(".sel-pick-" + m_sportsBetList.pick_list[i].bet_code + " .icon-iconLock").prop("hidden", false);
      }
    }
  }
}

function setBetPickOn(betCode) {
  if (!$(".odds-" + betCode).hasClass('pick')) {
    $(".odds-" + betCode).addClass('pick');
  }
}

function setBetPickOnAll() {
  for (let i = 0; i < m_sportsBetList.pick_list.length; i++) {
    setBetPickOn(m_sportsBetList.pick_list[i].bet_code);
  }
}

function selectPick(key, fixtureId, sportsCode, sportsName, betCode, rate, detail, selectIdx, pickDesc, marketName, title, marketId) {
  //해제인가 선택인가 판정
  let pos = getPickPosInBetCart(betCode);
  if (pos == -1) {

    let newRate = $(".odds-" + betCode + " .odds").html();
    if (key == '80567' || key == '80568' || key == '80569' || key == '80570' || key == '80571' || key == '80572') {
      for (let i = 0; i < m_sportsBetList.pick_list.length; i++) {
        if (m_sportsBetList.pick_list[i].fixture_id < 100) {
          deletePickInfo(m_sportsBetList.pick_list[i].bet_code);
        }
      }
      if (key == '80567') newRate = $("#three-bonus .odds").html();
      if (key == '80568') newRate = $("#five-bonus .odds").html();
      if (key == '80569') newRate = $("#seven-bonus .odds").html();
      if (key == '80570') newRate = $("#four-bonus .odds").html();
      if (key == '80571') newRate = $("#six-bonus .odds").html();
      if (key == '80572') newRate = $("#nine-bonus .odds").html();
    }

    let pickInfo = new PickInfo(fixtureId, betCode, newRate, pickDesc, detail, marketName, title, key, 0, marketId, selectIdx, sportsCode, sportsName);
    m_sportsBetList.game_type = game_type;
    // 같은 준위 베팅픽 삭제
    if (marketId != 0) {
      deleteSameLevelPick(fixtureId, marketId);
    }

    let tmpPickList = m_sportsBetList.pick_list.slice();
    tmpPickList.push(pickInfo);
    let bonusPickCnt = 0;
    let mapCheckBonus = new Map();
    let mapCheckRateByBetCode = new Map();
    let mapCheckRateByMarketId = new Map();
    let mapCheckMarketIds = new Map();
    let mapStrCheckMarketIds = new Map();
    let mapSportsCodeByFixtureId = new Map();
    let mapSportsNameBySportsCode = new Map();
    for (let i = 0; i < tmpPickList.length; i++) {
      let strPickDetail = tmpPickList[i].pick_detail;
      let strBetCode = tmpPickList[i].bet_code;
      let pickFixtureId = tmpPickList[i].fixture_id;
      let pickMarketId = tmpPickList[i].marketId;
      let pickSportsCode = tmpPickList[i].sports_code;
      let pickSportsName = tmpPickList[i].sports_name;
      if (pickSportsCode != '') {
        mapSportsCodeByFixtureId.set(pickFixtureId, pickSportsCode);
      }
      if (pickSportsCode != '' && pickSportsName != '') {
        mapSportsNameBySportsCode.set(pickSportsCode, pickSportsName);
      }
      if (strPickDetail.includes("Bonus") == true) {
        bonusPickCnt++;
        if (mapCheckBonus.has(strPickDetail) == true) {
          showMsg("보너스배당이 중복 선택되었습니다.", 'error');
          setBetPickStatusBySpBetCode(strBetCode, 1);
          return;
        }
        else {
          mapCheckBonus.set(strPickDetail, 1);
        }
      }
      else {
        if (mapCheckRateByBetCode.has(strBetCode) == true) {
          showMsg("같은 배당이 2번이상 선택되었습니다.", "error");
          setBetPickStatusBySpBetCode(strBetCode, 1);
          return;
        }
        else {
          mapCheckRateByBetCode.set(strBetCode, 1);
        }

        let marketKey = pickFixtureId + "_" + pickMarketId;
        if (mapCheckRateByMarketId.has(marketKey) == true) {
          showMsg("같은 마켓 배당이 2개 이상 선택되었습니다.", "error");
          setBetPickStatusBySpBetCode(strBetCode, 1);
          return;
        }
        else {
          mapCheckRateByMarketId.set(marketKey, 1);
        }
        if (mapCheckMarketIds.has(pickFixtureId) == true) {
          mapCheckMarketIds.get(pickFixtureId).push(pickMarketId);
        }
        else {
          let tmpList = new Array();
          tmpList.push(pickMarketId);
          mapCheckMarketIds.set(pickFixtureId, tmpList);
        }

        let strPushMarketId = pickMarketId.toString();
        if (tmpPickList[i].select_idx == 2) {
          strPushMarketId = 'x';
        }
        if (mapStrCheckMarketIds.has(pickFixtureId) == true) {
          mapStrCheckMarketIds.get(pickFixtureId).push(strPushMarketId);
        }
        else {
          let tmpList = new Array();
          tmpList.push(strPushMarketId);
          mapStrCheckMarketIds.set(pickFixtureId, tmpList);
        }
      }
    }
    if (bonusPickCnt > 1) {
      showMsg("보너스배당이 중복 선택되었습니다.", "error");
      return;
    }
    let checkSportsName = '';
    let marketLimitNotMatchYn = 0;
    let strErrorMsg = '';
    if (game_type == 1) strErrorMsg += ' - 국내형';
    else if (game_type == 2) strErrorMsg += ' - 해외형';
    else if (game_type == 3) strErrorMsg += ' - 라이브';
    else if (game_type == 4) strErrorMsg += ' - 실시간';
    else if (game_type == 5) strErrorMsg += ' - 스페셜';
    strErrorMsg += ' 에서 승인되지 않은 베팅조합입니다.';

    mapCheckMarketIds.forEach((value, key) => {
      if (value.length > 1) {
        let agreeInOneGamePossibleYn = 0;

        let tmpSportsCode = mapSportsCodeByFixtureId.get(key);
        checkSportsName = mapSportsNameBySportsCode.get(tmpSportsCode);
        let allowMarketLimitIdsList = mapAllowMarketLimitIds.get(tmpSportsCode);
        let strAllowMarketLimitIdsList = mapStrAllowMarketLimitIds.get(tmpSportsCode);
        let limitUseYn = mapMarketLimitUseYn.get(tmpSportsCode);

        if (limitUseYn == 1) {
          if (allowMarketLimitIdsList && allowMarketLimitIdsList.length > 0) {
            for (let i = 0; i < allowMarketLimitIdsList.length; i++) {
              let findYn = 0;
              let allowMarketLimitIds = allowMarketLimitIdsList[i];
              if (allowMarketLimitIds && allowMarketLimitIds.length > 0) {
                if (value.length > allowMarketLimitIds.length) continue;

                for (let j = 0; j < value.length; j++) {
                  if (allowMarketLimitIds.includes(value[j]) == false) {
                    findYn = 1;
                    break;
                  }
                }
                if (findYn == 0) {
                  agreeInOneGamePossibleYn = 1;
                  break;
                }
              }
            }
          }

          let strCheckMarketIds = mapStrCheckMarketIds.get(key);
          if (strAllowMarketLimitIdsList && strAllowMarketLimitIdsList.length > 0) {
            for (let i = 0; i < strAllowMarketLimitIdsList.length; i++) {
              let findYn = 0;
              let strAllowMarketLimitIds = strAllowMarketLimitIdsList[i];
              if (strAllowMarketLimitIds && strAllowMarketLimitIds.length > 0) {
                if (strCheckMarketIds.length > strAllowMarketLimitIds.length) continue;
                let inputXCnt = 0;
                let limitXCnt = 0;
                for (let k = 0; k < strAllowMarketLimitIds.length; k++) {
                  if (strAllowMarketLimitIds[k] == 'x') limitXCnt++;
                }
                for (let k = 0; k < strCheckMarketIds.length; k++) {
                  if (strCheckMarketIds[k] == 'x') inputXCnt++;
                }
                if (inputXCnt > limitXCnt) continue;

                for (let j = 0; j < strCheckMarketIds.length; j++) {
                  if (strAllowMarketLimitIds.includes(strCheckMarketIds[j]) == false) {
                    findYn = 1;
                    break;
                  }
                }
                if (findYn == 0) {
                  agreeInOneGamePossibleYn = 1;
                  break;
                }
              }
            }
          }

          if (agreeInOneGamePossibleYn == 0) {
            marketLimitNotMatchYn = 1;
            return;
          }
        }
      }
    });
    if (marketLimitNotMatchYn == 1) {
      strErrorMsg = checkSportsName + strErrorMsg;
      showMsg(strErrorMsg, "warning");
      return;
    }
    var mapNotAllowPossibleList = new Map();
    mapCheckMarketIds.forEach((value, key) => {
      let tmpSportsCode = mapSportsCodeByFixtureId.get(key);
      checkSportsName = mapSportsNameBySportsCode.get(tmpSportsCode);
      let tmpCheckList = mapNotAllowPossibleList.get(tmpSportsCode);
      if (tmpCheckList && tmpCheckList.length > 0) {
        let newCheckList = new Array();
        for (let i = 0; i < value.length; i++) {
          for (let j = 0; j < tmpCheckList.length; j++) {
            let oldList = tmpCheckList[j];
            let copyList = new Array();
            for (let k = 0; k < oldList.length; k++) {
              copyList.push(oldList[k]);
            }
            copyList.push(value[i]);
            newCheckList.push(copyList);
          }
        }
        tmpCheckList = newCheckList;
      }
      else {
        tmpCheckList = new Array();
        for (let i = 0; i < value.length; i++) {
          let tmpList = new Array();
          tmpList.push(value[i]);
          tmpCheckList.push(tmpList);
        }
      }
      mapNotAllowPossibleList.set(tmpSportsCode, tmpCheckList);
    });
    mapNotAllowPossibleList.forEach((value, key) => {
      let notAgreeInOtherGameFindYn = 0;
      checkSportsName = mapSportsNameBySportsCode.get(key);
      let notAllowMarketLimitIdsList = mapNotAllowMarketLimitIds.get(key);
      if (notAllowMarketLimitIdsList && notAllowMarketLimitIdsList.length > 0) {
        for (let i = 0; i < notAllowMarketLimitIdsList.length; i++) {
          notAllowMarketLimitIds = notAllowMarketLimitIdsList[i];
          for (let k = 0; k < value.length; k++) {
            let findYn = 0;
            let inputMarketIds = value[k];
            if (notAllowMarketLimitIds.length <= inputMarketIds.length) {
              for (let j = 0; j < notAllowMarketLimitIds.length; j++) {
                if (inputMarketIds.includes(notAllowMarketLimitIds[j]) == false) {
                  findYn = 1;
                  break;
                }
              }
              if (findYn == 0) {
                notAgreeInOtherGameFindYn = 1;
                break;
              }
            }
          }
          if (notAgreeInOtherGameFindYn == 1) {
            break;
          }
        }
        if (notAgreeInOtherGameFindYn == 1) {
          marketLimitNotMatchYn = 1;
          return;
        }
      }
    });
    if (marketLimitNotMatchYn == 1) {
      strErrorMsg = checkSportsName + strErrorMsg;
      showMsg(strErrorMsg, "warning");
      return;
    }
    //베팅카트 추가
    m_sportsBetList.pick_list.push(pickInfo);
    if (getQuickBetStatus() == 1) {
      sendBetCart();
      return;
    }
    rebuildBetCart();
    setBetPickOn(betCode);
  } else {
    deletePickInfo(betCode);
  }
}

function checkBetCartRate(t) {
  ajaxSend('/front/game/sports/update_pick.asp'
    , {
      game_type: m_sportsBetList.game_type
      , pick_list: JSON.stringify(m_sportsBetList.pick_list)
    }
    , function (jData) {
      if (!jData.success) {
        showMsg(jData.message || "배당갱신 실패하였습니다.", 'error');
        return;
      }
      console.log(jData);
      if (jData.data) {
        for (let i = 0; i < jData.data.length; i++) {
          for (let j = 0; j < m_sportsBetList.pick_list.length; j++) {
            if (m_sportsBetList.pick_list[j].bet_code == jData.data[i].bet_code) {
              if (m_sportsBetList.pick_list[j].select_rate != jData.data[i].select_rate) {
                m_sportsBetList.pick_list[j].old_rate = m_sportsBetList.pick_list[j].select_rate;
              }
              m_sportsBetList.pick_list[j].select_rate = jData.data[i].select_rate;
              m_sportsBetList.pick_list[j].status = jData.data[i].status;
            }
          }
        }
        rebuildBetCart();
      }
      if(!t)showMsg("업데이트 성공하였습니다.", "success");
    });
}

function betting_ready_popup() {
  $("#betLoading").fadeIn();
}

function betting_ready_popup_close() {
  $("#betLoading").fadeOut("fast");
}

var bettingIntervalId = null;
var varIntervalCounter = 0;
var varFuncName = function () {
  if (varIntervalCounter <= 300) {
    varIntervalCounter++;
    $("#betLoading #bettingSecond").html(varIntervalCounter);
  } else {
    betting_ready_popup_close();
    clearInterval(bettingIntervalId);
  }
};

var varFuncDelay = function () {
  if (varIntervalCounter <= bet_delay) {
    varIntervalCounter++;
    $("#betLoading #bettingSecond").html(varIntervalCounter);
  } else {
    betting_ready_popup_close();
    clearInterval(bettingIntervalId);
    sendSportsBetJson();
  }
};

function sendBetCart() {
  if (betConfirm) {
    if (betConfirmMsg == 'true') {
      confirmMsgYn('지금 배팅하시겠습니까?', function () {
        sendBetDelay()
      });
    } else {
      sendBetDelay();
    }
  }
}

function sendBetDelay() {
  betConfirm = false;
  if (m_sportsBetList.game_type == 3) {
    betting_ready_popup();
    varIntervalCounter = 0;
    $("#betLoading #bettingSecond").html(0);
    bettingIntervalId = setInterval(varFuncDelay, 1000);
  } else {
    betting_ready_popup_close();
    clearInterval(bettingIntervalId);
    sendSportsBetJson();
  }
}

function sendSportsBetJson() {
  let betCash = 0;
  if (getQuickBetStatus() == 0) {
    betCash = parseCommaInteger($('#betting_cart .monitor .state-wrap').find("input[name=input_bet_cash]").val() || 0);
  }
  else {
    betCash = quickBetCash;
  }
  betting_ready_popup();
  varIntervalCounter = 0;
  $("#betLoading #bettingSecond").html(0);
  bettingIntervalId = setInterval(varFuncName, 1000);
  ajaxSend('/front/game/sports/bet.asp'
    , {
      bet_cash: betCash
      , game_type: m_sportsBetList.game_type
      , pick_list: JSON.stringify(m_sportsBetList.pick_list)
    }
    , function (jData) {
      betting_ready_popup_close();
      clearInterval(bettingIntervalId);

      if (!jData.success) {
        showMsg(jData.message || "베팅 실패하였습니다.", 'error');
        if (jData.data) {
          setBetPickStatusBySpBetCode(jData.data, 1);
          showBetPickStatus();
        }
        betConfirm = true;
        return;
      }
      showMsg("베팅 성공하였습니다.", "success");
      clearBetCart();
      if (typeof initStartPage == 'function') {
        initStartPage();
      }
      if (getQuickBetStatus() == 0) {
        bettingMoneyPlus(jData.data.minBetCash);
      }
      $('.userCashAmount').each(function () {
        if ($(this).prop("tagName") == 'INPUT') {
          $(this).val(formatComma(jData.data.myCash));
          return;
        }
        $(this).html(formatComma(jData.data.myCash));
      });
      getBettingList();
      betConfirm = true;
    });
}

function clearBetCart() {
  m_sportsBetList.game_type = 0;
  m_sportsBetList.pick_list = [];
  $("#betting_cart_opener b").html(m_sportsBetList.pick_list.length);
  $("#betting_cart .bet-cart-cnt").html(m_sportsBetList.pick_list.length);
  $("#betting_cart .betting-pick-option .folder-wrap").empty();
  $(".bet").removeClass("pick");
  $(".bonus-bet").removeClass("pick");
  //initBetValue(-1, -1, -1, -1, -1, -1);
  initBetValue();
}

function setBetPickStatus(rateDetailInfo, status) {
  if (m_sportsBetList.pick_list.length > 0) {
    for (let i = 0; i < m_sportsBetList.pick_list.length; i++) {
      if ((rateDetailInfo.abetCode != '' && rateDetailInfo.abetCode == m_sportsBetList.pick_list[i].bet_code)
        || (rateDetailInfo.dbetCode != '' && rateDetailInfo.dbetCode == m_sportsBetList.pick_list[i].bet_code)
        || (rateDetailInfo.hbetCode != '' && rateDetailInfo.hbetCode == m_sportsBetList.pick_list[i].bet_code)
      ) {
        m_sportsBetList.pick_list[i].status = status;
      }
    }
  }
}

function deletePickInfo(betCode) {
  removePickFromBetCart(betCode);
  rebuildBetCart();
}

function PickInfo(pFixtureId, pBetCode, pSelectRate, pSelectPickDesc, pPickDetail, pMarketName, pPickTitle, pKey, pStatus, pMarketId, pSelectIdx, pSportsCode, pSportsName) {
  this.fixture_id = pFixtureId;
  this.bet_code = pBetCode;
  this.select_rate = pSelectRate;
  this.select_pick_desc = pSelectPickDesc;
  this.pick_detail = pPickDetail;
  this.market_name = pMarketName;
  this.pick_title = pPickTitle;
  this.key = pKey;
  this.status = pStatus;
  this.marketId = pMarketId;
  this.select_idx = pSelectIdx;
  this.sports_code = pSportsCode;
  this.sports_name = pSportsName;
  this.old_rate = '';
}

PickInfo.prototype.fixture_id;
PickInfo.prototype.bet_code;
PickInfo.prototype.select_rate;
PickInfo.prototype.select_pick_desc;
PickInfo.prototype.pick_detail;
PickInfo.prototype.pick_title;
PickInfo.prototype.key;
PickInfo.prototype.status;
PickInfo.prototype.marketId;
PickInfo.prototype.select_idx;
PickInfo.prototype.sports_code;
PickInfo.prototype.sports_name;
PickInfo.prototype.market_name;
PickInfo.prototype.old_rate

function BetList() {
  this.pick_list = new Array();
}

BetList.prototype.bet_cash;
BetList.prototype.game_type;
BetList.prototype.pick_list;

var quickBetStatus = false;
var quickBetCash = 0;

function initQuickBetCash(minCash) {
  quickBetCash = minCash;
  $(".quickbetting .top .money input[name=money]").val(formatComma(minCash));
}

function initQuickBetSwitch() {
  if (getQuickBetStatus() == 1) {
    $(".quickbetting .quick-bet-switch").addClass("accordion-opened");
    $(".quickbetting > .top .money").addClass("show");
    quickBetStatus = true;
    $('#betting_cart .monitor .state-wrap').find("input[name=input_bet_cash]").addClass("disabled");
  }
  else {
    $(".quickbetting .quick-bet-switch").removeClass("accordion-opened");
    $(".quickbetting > .top .money").removeClass("show");
    quickBetStatus = false;
    $('#betting_cart .monitor .state-wrap').find("input[name=input_bet_cash]").removeClass("disabled");
  }
}

function switchQuickBetStatus() {
  if (quickBetStatus == true) {
    setSessionStorage('sportsQuickBet', 0);
    $(".quickbetting > .top .money").removeClass("show");
    quickBetStatus = false;
    $('#betting_cart .monitor .state-wrap').find("input[name=input_bet_cash]").removeClass("disabled");
  }
  else {
    setSessionStorage('sportsQuickBet', 1);
    $(".quickbetting > .top .money").addClass("show");
    quickBetStatus = true;
    $('#betting_cart .monitor .state-wrap').find("input[name=input_bet_cash]").addClass("disabled");
  }
}

function switchMobQuickBetStatus() {
  switchQuickBetStatus();
  if (quickBetStatus == true) {
    $("#right-container .quickbetting .quick-bet-switch").addClass("accordion-opened");
  }
  else {
    $("#right-container .quickbetting .quick-bet-switch").removeClass("accordion-opened");
  }
}

function switchPCQuickBetStatus() {
  switchQuickBetStatus();
  if (quickBetStatus == true) {
    $(".mob-main-header .quickbetting .quick-bet-switch").addClass("accordion-opened");
  }
  else {
    $(".mob-main-header .quickbetting .quick-bet-switch").removeClass("accordion-opened");
  }
}

function getQuickBetStatus() {
  return getSessionStorage('sportsQuickBet') | 0;
}