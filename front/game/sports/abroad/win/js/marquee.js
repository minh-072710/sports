$.fn.extend({
  marquee: Marquee,
});

function Marquee(content, duration) {
  let newMarquee = $(`
    <style>
      [origin], [clone] {
        min-width: 200%;
      }
    </style>
    <div origin>${content}</div>
    <div clone>${content}</div>
  `);
  this.find("[marquee-content]").css({
    width: "100%",
    display: "flex",
  });
  this.find("[marquee-content]").html(newMarquee);
  function animate() {
    $({ keyframe: 0 }).animate(
      { keyframe: 100 },
      {
        duration: duration,
        easing: "linear",
        step: function (keyframe) {
          newMarquee.css({
            transform: `translate(-${keyframe}%)`,
          });
        },
        complete: function () {
          animate();
        },
      }
    );
  }
  animate();
}