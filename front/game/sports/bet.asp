<!--#include virtual="/include/dbcon.asp"-->
<!--#include virtual="/include/jsonObject.class.asp"-->

<%
response.ContentType = "application/json"

' 유저정보
SQL = "select * from tb_member where mem_id = '"& Session("IU_ID") &"'"
Set RS=dbcon.Execute(sql)
If Not RS.eof Then
  mem_idx = rs("idx")
  g_money = rs("g_money")
  userGrade = rs("grade")
  point = rs("point")
  userLev = rs("lev")
  canBetting = rs("canBetting")
  max_bet = rs("max_bet")
  max_mix_bet = rs("max_mix_bet")
else 
  Response.write "{""success"":false,""message"":""로그인를 해주세요.""}"
  Response.end
End if
rs.close  

If canBetting = "N" Then
  Response.write "{""success"":false,""message"":""현재 베팅이 불가능합니다. 고객센터에 문의바랍니다.""}"
  Response.end
End if

' 베팅제한 정보
sql = "select * from tb_config_bet where lev = '"& userGrade &"'"
Set RS=dbcon.Execute(sql)
If Not RS.eof Then
  max_prize = rs("max_prize")
  max_bet = rs("max_bet")
  min_bet = rs("min_bet")
  max_prize_folder = rs("max_prize_folder")
  max_bet_folder = rs("max_bet_folder")
  min_bet_folder = rs("min_bet_folder")
End if
rs.close

' 경기시간 체크
current_time = now()
user_ip = getUserIp()
mybet_momey = request.form("bet_cash")
game_kind = request.form("game_type")
betdata = request.form("pick_list")

if mybet_momey="" or betdata="" then
  Response.write "{""success"":false,""message"":""현재 베팅이 불가능합니다. 고객센터에 문의바랍니다.""}"
  Response.end
end if

if CDbl(g_money)<CDbl(mybet_momey) then
  Response.write "{""success"":false,""message"":""보유머니가 부족합니다.""}"
  Response.end
end if

set jsonObj = new JSONobject
set outputObj = jsonObj.parse(betdata) 
'mybet_rate = betdatas(1)
bet_count = outputObj.length
protoID = createGuid()
mybet_rate = 1

if bet_count=0 then
  Response.write "{""success"":false,""message"":""베팅을 선택해 주세요.""}"
  Response.end
end if

if bet_count>10 then
  Response.write "{""success"":false,""message"":""최대 베팅 폴더 수는 10입니다.""}"
  Response.end
end if

if bet_count>1 then
  max_prize = max_prize_folder
  max_bet = max_bet_folder
  min_bet = min_bet_folder
end if

if CDbl(mybet_momey)<CDbl(min_bet) then
  Response.write "{""success"":false,""message"":""최소 베팅은 "& formatnumber(min_bet,0) &"원 입니다.""}"
  Response.end
end if

if CDbl(mybet_momey)>CDbl(max_bet) then
  Response.write "{""success"":false,""message"":""최대 베팅은 "& formatnumber(max_bet,0) &"원 입니다.""}"
  Response.end
end if

Dbcon.BeginTrans

' 체크
For i = 0 To bet_count-1
  Set this = outputObj(i)

  fixture_idx = this("fixture_id")
  bet_idx = this("bet_code")
  mybet_rate = mybet_rate * CDbl(this("select_rate"))

  ' 베팅

  SQL = "select * from tb_parent where status=1 and isStop = 'N' and fixture_idx = '"& fixture_idx &"' and (betid1 = '"& bet_idx &"' or betid2 = '"& bet_idx &"' or betid3 = '"& bet_idx &"'); "
  Set Rs = dbcon.Execute(sql)
  If Rs.eof Then
    dbcon.RollBackTrans
    dbcon.close
    Response.write "{""success"":false,""message"":""베팅시간이 마감되었습니다."",""data"":"""& bet_idx &"""}"
    response.end
  Else
    betid1 = Rs("betid1")
    betid2 = Rs("betid2")
    betid3 = Rs("betid3")
    origin_rate1 = rs("rate1")
    origin_rate2 = rs("rate2")
    origin_rate3 = rs("rate3")
    temp_rate1 = rs("add_rate1")
    temp_rate2 = rs("add_rate2")
    temp_rate3 = rs("add_rate3")
    parent_idx = Rs("idx")
    select_score = Rs("vs_team_sub")
    game_time = Rs("game_time")
    game_id = Rs("game_id")
    home_team = Rs("home_team")
    game_type = Rs("game_type")
  End If
  Rs.close

  if origin_rate1="" then origin_rate1="0"
  if origin_rate2="" then origin_rate2="0"
  if origin_rate3="" then origin_rate3="0"
  rate1 = origin_rate1
  rate2 = origin_rate2
  rate3 = origin_rate3

  if not isnumeric(rate1) then rate1=0
  if not isnumeric(rate2) then rate2=0
  if not isnumeric(rate3) then rate3=0
  If CDbl(rate1) > CDbl(rate3) Then
    add_rate1 = temp_rate1
    add_rate2 = temp_rate2
    add_rate3 = temp_rate3
  ElseIf CDbl(rate1) < CDbl(rate3) Then
    add_rate1 = temp_rate1
    add_rate2 = temp_rate2
    add_rate3 = temp_rate3
  Else
    If game_type <> 1 And game_type <> 4 And game_type <> 51 Then
      add_rate1 = temp_rate3
      add_rate2 = temp_rate2
      add_rate3 = temp_rate3
    Else
      add_rate1 = 0
      add_rate2 = temp_rate2
      add_rate3 = 0
    End if
  End if
        
  total_rate1 = rate1 + CDbl(add_rate1)
  total_rate2 = rate2 + CDbl(add_rate2)
  total_rate3 = rate3 + CDbl(add_rate3)

  If betid1 = bet_idx Then
    choice = 1
    select_rate = total_rate1
  ElseIf betid2 = bet_idx Then
    choice = 2
    select_rate = total_rate2
  ElseIf betid3 = bet_idx Then
    choice = 3
    select_rate = total_rate3
  End If
  

  if CDbl(select_rate)<>CDbl(this("select_rate")) then 
    dbcon.RollBackTrans
    dbcon.close
    Response.write "{""success"":false,""message"":""배당이 변경되었습니다."",""data"":"""& bet_idx &"""}"
    response.end
  end if

  '베팅 - total 베팅
  sql = "insert into tb_total_betting (sports_kind, parent_idx, game_type, game_no, GameSelect, rate1, rate2, rate3, select_rate, select_line, select_score, result, betid) values('','"& parent_idx &"','"& game_type &"','"& protoID &"', '"& choice &"','"& total_rate1 &"','"& total_rate2 &"','"& total_rate3 &"','"& select_rate &"','"& origin_rate2 &"','"& select_score &"','0', '"& bet_idx &"')"
  dbcon.execute sql

  If userLev = "1" Or userLev = "2" Then
    sql = "update tb_parent set money"& choice &" = money"& choice &" + "& mybet_momey & " where idx = '"& parent_idx &"'"
    dbcon.execute sql
  End If

Next
mybet_rate = round(mybet_rate,2)
if CDbl(mybet_momey)*CDbl(mybet_rate)>CDbl(max_prize) then
  Response.write "{""success"":false,""message"":""최대 당첨은 "& formatnumber(max_prize,0) &"원 입니다.""}"
  Response.end
end if

total_money = g_money-mybet_momey
'베팅 - total 카트
sql = "insert into tb_total_cart (mem_idx, game_no, regdate, result, betting_cnt, betting_money, result_rate, result_money, visible, bettingIP, confirmBetting, reason, sports_type, game_type) values('"& mem_idx &"','"& protoID &"',getdate(),'0', '"& bet_count &"','"& mybet_momey &"','"& mybet_rate &"','0','Y','"& user_ip &"', '0', '', 'sports', '"& game_kind &"')"
dbcon.execute sql

'보유금액 차감
sql = "update tb_member set g_money = g_money - " & mybet_momey & " where idx = '"& mem_idx &"'"
dbcon.execute sql

'포인트 내역 추가
sql = "insert into tb_point (memidx, kubun, code, amount_money, amount_point, g_jan, p_jan, content, reason, regdate) values('"& mem_idx &"', '보유금액차감', 2, '"& mybet_momey &"', 0, '"& total_money &"', '"& point &"', '베팅구매', '"& protoID &"', getdate())"
dbcon.execute sql

If dbcon.Errors.Count=0 Then
  dbcon.CommitTrans      
  dbcon.close
  Response.write "{""success"":true,""data"":{""minBetCash"":"& min_bet &",""myCash"":"& total_money &"}}"
Else
  dbcon.RollBackTrans
  dbcon.close
  Response.write "{""result"":false,""message"":""시스템 오류""}"
End If  
Response.End
%>