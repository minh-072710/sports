<!--#include virtual="/include/dbcon.asp"-->
<%
  mem_id = Session("IU_ID")
  game_type = ReForm("type")
  pagestart = ReForm("start")
  pagesize = ReForm("size")
  If pagestart="" Then pagestart = "0"
  If pagesize="" Then pagesize = "10"
  
  start_date = DateAdd("n", -30, now())
  start_date = formatdatetime(start_date, 2) & " " & formatdatetime(start_date, 4)
  end_date = DateAdd("n", 3, now())
  end_date = formatdatetime(end_date, 2) & " " & formatdatetime(end_date, 4)

  duration = "'"& start_date &"' and '"& end_date &"'"
  sqlOp = " and t.visible='Y' and m.mem_id = '"& Session("IU_ID")& "'" 
  if game_type<>"" then sqlOp = sqlOp & " and t.game_type = '"& game_type &"'" 
  sql = "select count(*) as recCount from tb_total_cart t with(nolock) left join tb_member m with(nolock) on t.mem_idx = m.idx where t.regdate between "&duration&" and t.sports_type='sports'" & sqlOp 
  'sql = "select count(*) as recCount from tb_total_cart t with(nolock) left join tb_member m with(nolock) on t.mem_idx = m.idx where t.sports_type='sports'" & sqlOp 
  'response.write sql & "<br>"
  set Rs = dbcon.execute(sql)
  TotRecord = Rs("recCount")

  sql = "select c.isCanceled, b.fixture_idx, c.idx as id, c.game_type, c.regdate as bet_date, c.result, c.betting_money, c.result_rate,"
  sql = sql & " a.result as bet_result, a.select_score, a.rate1 as my_rate1, a.rate2 as my_rate2, a.rate3 as my_rate3,"
  sql = sql & " b.game_time, a.GameSelect, b.home_team, b.away_team, b.game_type, k.name as marketname"
  sql = sql & " from tb_total_betting a with(nolock)"
  sql = sql & " left join tb_total_cart c with(nolock) on a.game_no = c.game_no"
  sql = sql & " left join tb_parent b with(nolock) on a.parent_idx = b.idx"
  sql = sql & " left join tb_market k with(nolock) on k.idx = b.game_id"
  sql = sql & " where a.game_no in"
  sql = sql & " (select game_no from tb_total_cart t left join tb_member m on t.mem_idx = m.idx where"
  sql = sql & " t.regdate between "&duration&" and"
  sql = sql & " t.sports_type='sports'" & sqlOp 
  sql = sql & " order by t.idx asc"
  sql = sql & " offset "& pagestart &" rows fetch next "& pagesize &" rows only)"
  sql = sql & " order by c.idx asc"
  set Rs = dbcon.execute(sql)
  while not Rs.eof
    id = Rs("id")
    bet_date = Rs("bet_date")
    cart_result = Rs("result")
    bet_money = Rs("betting_money")
    bet_rate = CDbl(Rs("result_rate"))
    game_type = Rs("game_type")
    bonus_rate = 0
    select case cart_result
      case "1"
        cart_result = "2"
      case "2"
        cart_result = "1"
      case "3"
        cart_result = "4"
      case else
        cart_result = "0"
    end select
    if Rs("isCanceled")="Y" then cart_result="5"

    If pre_id <> id Then
      cntInfo = cntInfo & "{""id"":"""& id &""""
      cntInfo = cntInfo & ",""betDate"":"""& bet_date &""""
      cntInfo = cntInfo & ",""status"":"& cart_result
      cntInfo = cntInfo & ",""cashBet"":"& bet_money
      cntInfo = cntInfo & ",""rateBet"":"""& FormatNumber(bet_rate, 2, -1, -1, -1) &""""
      cntInfo = cntInfo & ",""rateBonus"":"""& bonus_rate &""""
      cntInfo = cntInfo & "}"
    end if

    fixture_idx = Rs("fixture_idx")
    game_time = Rs("game_time")
    bet_result = Rs("bet_result")
    bet_rate = Rs("result_rate")
    rate1 = getRateFormat(rs("my_rate1"))
    rate2 = getRateFormat(rs("my_rate2"))
    rate3 = getRateFormat(rs("my_rate3"))
    game_select = rs("gameSelect")
    home_team = Rs("home_team")
    away_team = Rs("away_team")
    market = Rs("marketname")
    select_score = Rs("select_score")
    base_line = ""
    select_rate = rate1
    selectIdx = "3"
    if game_type="3" then selPickName = "언더" else selPickName = "홈승"
    if game_type="10" then selPickName = home_team_sub
    if game_select="2" then 
      select_rate = rate2
      selectIdx = "2"
      selPickName = "무승"
    end if
    if game_select="3" then 
      select_rate = rate3
      selectIdx = "1"
      if game_type="3" then selPickName = "오버" else selPickName = "원정승"
      if game_type="10" then selPickName = away_team_sub
    end if
    if game_type<>"1" and game_type<>"10" then 
      base_line=rate2
      if game_type="2" and game_select="3" then base_line=formatnumber(-1*CDbl(rate2),1)
      base_line=base_line&select_score
    end if
    select case bet_result
      case "1"
        bet_result = "2"
      case "2"
        bet_result = "1"
      case "3"
        bet_result = "4"
      case else
        bet_result = "0"
    end select
    if Rs("isCanceled")="Y" then bet_result="5"
    gameType = "크로스"
    if game_type="2" then gameType = "프리매치"
    if game_type="3" then gameType = "인플레이"

    detailInfo = detailInfo & "{""betId"":"""& id &""""
    detailInfo = detailInfo & ",""status"":"& bet_result
    detailInfo = detailInfo & ",""fixtureId"":"""& fixture_idx & """"
    detailInfo = detailInfo & ",""selectRate"":"""& select_rate &""""
    detailInfo = detailInfo & ",""baseLine"":"""& base_line &""""
    detailInfo = detailInfo & ",""startDate"":"""& game_time &""""
    detailInfo = detailInfo & ",""selectIdx"":"& selectIdx
    detailInfo = detailInfo & ",""selPickName"":"""& selPickName &""""
    detailInfo = detailInfo & ",""homeTeamName"":"""& home_team &""""
    detailInfo = detailInfo & ",""awayTeamName"":"""& away_team &""""
    detailInfo = detailInfo & ",""marketName"":"""& market &""""
    detailInfo = detailInfo & ",""gameType"":"""& gameType &""""
    detailInfo = detailInfo & "}"

    Rs.movenext
    if not Rs.eof then
      detailInfo = detailInfo & ","
      if id<>Rs("id") then cntInfo = cntInfo & ","
    end if
    pre_id = id
  wend

  data = "{""data"": {""list"":["& cntInfo &"],""detailList"":["& detailInfo & "],""length"":"& pagesize &",""total"":"& TotRecord &"},""success"":true}"
  response.ContentType = "application/json"
  response.write data
%>